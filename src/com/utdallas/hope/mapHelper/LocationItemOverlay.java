/*
 package com.utdallas.hope.mapHelper;


import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class LocationItemOverlay extends ItemizedOverlay {

	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	Context mContext;
	
	public LocationItemOverlay(Drawable defaultMarker, Context context) {
		  super(defaultMarker);
		  mContext = context;
		}
	
	public LocationItemOverlay(Drawable arg0) {
		super(boundCenterBottom(arg0));
	}

	@Override
	protected OverlayItem createItem(int arg0) {
		
		return mOverlays.get(arg0);

	}

	@Override
	public int size() {
		
		return mOverlays.size();

	}

	public void addOverlay(OverlayItem overlay) {
	    mOverlays.add(overlay);
	    populate();
	}
	
	@Override
	protected boolean onTap(int index) {
	  OverlayItem item = mOverlays.get(index);
	  return true;
	}
	
}
 */