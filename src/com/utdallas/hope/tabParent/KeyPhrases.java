package com.utdallas.hope.tabParent;

import java.util.Locale;

import android.app.TabActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.controller.FeaturesActivity;
import com.utdallas.hope.controller.KeyPhraseThingsActivity;
import com.utdallas.hope.dialogDisplay.Last5Dialog;
import com.utdallas.hope.gridModel.KeyPhraseThings;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.menuFunction.Search;

public class KeyPhrases extends TabActivity implements TextToSpeech.OnInitListener {
	/** Called when the activity is first created. */
	private TabHost mTabHost;
	
	KeyPhraseThingsActivity kpthingsActivityObject=new KeyPhraseThingsActivity();
	
	TabSpec actionsTabSpec;
	TabSpec thingsTabSpec;
	TextToSpeech mTts;
	
	TextView acc;
public Context keyphraseContext=this;
private static final int PREFERENCE = 12;

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data)
	    {
	    switch(requestCode) {
	    
	    case PREFERENCE:
	    	 SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
	    }
	    }


private void showConfiguration_settings() {
	  		// TODO Auto-generated method stub
	  			Intent intent = new Intent().setClass(this,Preferences.class);
	  			
	  			startActivityForResult(intent,PREFERENCE);
	  		
	  	}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);
    return true;
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
    case R.id.configuration_settings:
    	showConfiguration_settings();
        return true;
    case R.id.help:
        showHelp();
        return true;
    case R.id.search:
        showSearch();
        return true;
    case R.id.bookmark:
    	addToFavorites();
        return true;
    default:
        return super.onOptionsItemSelected(item);
    }
}
	private void addToFavorites() {
		boolean present=false; 
		Cursor cur= HopeApplication.db.query("favorites", null, null, null, null, null, null);
		while(cur.moveToNext())
		{
			if(cur.getString(1).equalsIgnoreCase(((TextView)findViewById(R.id.acc)).getText().toString()))
			{
				present=true;
				break;
			}
		}
		if(!present){
			ContentValues cv= new ContentValues();
			TextView t = (TextView)findViewById(R.id.acc);
			if(!t.getText().toString().equalsIgnoreCase("my sentence"))
			{
			cv.put("text" , t.getText().toString());
			HopeApplication.db.insert("favorites", null, cv);
			}
		}
	
}

	private void showSearch() {
		SentenceGeneration.clearAllApplicationVariables();
		Intent intent = new Intent().setClass(this,Search.class);
		
		startActivity(intent);
	
}

	private void showHelp() {
	// TODO Auto-generated method stub
	
}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communication);

		 mTts = new TextToSpeech(this,
		            this  
		            );
		
		 TextView tv= (TextView)findViewById(R.id.title);
			tv.setText("Key Phrases > Things");
		
        Button pronoun = (Button)findViewById(R.id.Pronoun_Button);
        pronoun.setVisibility(View.GONE);
		String extraParam=getIntent().getStringExtra("refresh");
		
		String searchTable = getIntent().getStringExtra("searchTable");
		
		ImageButton homeButton =(ImageButton)findViewById(R.id.home);
		homeButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				startActivity(intent);
				
			}

			});
		
		Button clearButton =(Button)findViewById(R.id.Clear_Button);
		clearButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				SentenceGeneration.clearAllApplicationVariables();

				finish();
        		Intent intent = new Intent().setClass(keyphraseContext,KeyPhrases.class);
				
			
				startActivity(intent);
			}

			});
		
		
		
		
		
		
		Button speakButton =(Button)findViewById(R.id.Speak_Button);
		speakButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				
				TextView t=(TextView)findViewById(R.id.acc);
				
				boolean present=false; boolean modified=true;
				Cursor cur= HopeApplication.db.query("added_sentences", null, null, null, null, null, null);
				while(cur.moveToNext())
				{
					if(cur.getString(1).equalsIgnoreCase(t.getText().toString()))
					{
						present=true;
						
					}
				}

				if(t.getText().toString().equalsIgnoreCase("My sentence") )
				{
					modified=false;
				}
				
				if(modified && !present)
				{
					
					ContentValues cv= new ContentValues();
					cv.put("sentence" , t.getText().toString());
				
					HopeApplication.db.insert("added_sentences", null, cv);
					
					
				}

				if(!t.getText().toString().equalsIgnoreCase("my sentence"))
					
				{
				 mTts.speak(t.getText().toString(),
				            TextToSpeech.QUEUE_FLUSH,  
				            null);
				if(!HopeApplication.recentSentences.contains(t.getText().toString()))
				{
				 if(HopeApplication.recentSentences.size()==5)
				 {
					 
					 
					 for(int i=0;i<4;i++)
					 {
						 HopeApplication.recentSentences.set(i, HopeApplication.recentSentences.get(i+1));
					 }
					 HopeApplication.recentSentences.remove(4);
					 HopeApplication.recentSentences.add(t.getText().toString());
					 
				 }
				 else
				 {
					 HopeApplication.recentSentences.add(t.getText().toString());
					 
				 }
				}
			}}});
		
		ImageButton last5Button =(ImageButton)findViewById(R.id.Last5_Button);
		last5Button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				//finish();
        		Intent intent = new Intent().setClass(keyphraseContext,Last5Dialog.class);
			
				startActivityForResult(intent, 2);
			}});
		
		
		
		mTabHost = getTabHost();
		
		if(extraParam==null)
		{	
		
		
Intent intent = new Intent().setClass(this, kpthingsActivityObject.getClass());
 	if(searchTable!=null && !searchTable.equals("")){
 		intent.putExtra("searchTable", searchTable);
 	}
thingsTabSpec=mTabHost.newTabSpec("kpthings").setIndicator("Key Phrases",
		getResources().getDrawable(R.drawable.ic_keyphrases))
.setContent(intent);
		mTabHost.addTab(thingsTabSpec);
		
		
		
		mTabHost.setCurrentTab(0);
		
		}
		else
		{
			
			Intent intent = new Intent().setClass(this, KeyPhraseThingsActivity.class);
			intent.putExtra("loadTable", "table");
			thingsTabSpec=mTabHost.newTabSpec("kpthings").setIndicator("Key Phrases",
					getResources().getDrawable(R.drawable.ic_keyphrases))
			.setContent(intent);
					mTabHost.addTab(thingsTabSpec);

			mTabHost.setCurrentTab(0);	
			
			
		}
		
		//whatever happens set the features tab as constant 

		
		Intent intent = new Intent().setClass(this, FeaturesActivity.class);
		thingsTabSpec=mTabHost.newTabSpec("features").setIndicator("Features",
				getResources().getDrawable(R.drawable.ic_features))
		.setContent(intent);
				mTabHost.addTab(thingsTabSpec);
				
				mTabHost.setCurrentTab(0);
	
		
		
		mTabHost.setOnTabChangedListener(new OnTabChangeListener(){
			@Override
			public void onTabChanged(String tabId) {
				 TextView tv= (TextView)findViewById(R.id.title);
				if(mTabHost.getCurrentTab()==1){
				
					tv.setText("Features (Key Phrases Active)");
				}else{
					tv.setText("Key Phrases > Things");
				}
			}});
		
		
		
	}

	@Override
	  public  boolean onKeyDown(int keyCode, KeyEvent event) {
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	// TODO Auto-generated method stub
	//super.onBackPressed();


	if (!HopeApplication.kpthings.isEmpty()) {
		if (mTabHost.getCurrentTabTag().equalsIgnoreCase( "Key Phrases")) {
			if (HopeApplication.kpthings.size() <= 1) {

			
				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
				thingsGrid.setAdapter(new KeyPhraseThings(
						thingsGrid.getContext(),
						getLayoutInflater(), "kpthings"));
			
				HopeApplication.kpthings.remove(HopeApplication.kpthings.get(HopeApplication.kpthings.size() - 1));
			

			} else {

				
				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
				thingsGrid.setAdapter(new KeyPhraseThings(
						thingsGrid.getContext(),
						getLayoutInflater(), HopeApplication.kpthings
								.get(HopeApplication.kpthings.size() - 1)));
			
				HopeApplication.kpthings.remove(HopeApplication.kpthings.get(HopeApplication.kpthings.size() - 1));
				
			}

		}
	} else {
//		Toast.makeText(KeyPhrases.this, "Cannot go back",
//				Toast.LENGTH_SHORT).show();

	}

}
		return true;

	}
	
	
	
	
	@Override
	public void onInit(int status) {
		
		// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
	    if (status == TextToSpeech.SUCCESS) {
	        
	        int result = mTts.setLanguage(Locale.US);
	        
	        if (result == TextToSpeech.LANG_MISSING_DATA ||
	            result == TextToSpeech.LANG_NOT_SUPPORTED) {
	           
	            
	        } else {
	        	
	           
	        }
	    } else {
	        // Initialization failed.
	        //Log.e(TAG, "Could not initialize TextToSpeech.");
	    	
	    }} 
	
	
}
