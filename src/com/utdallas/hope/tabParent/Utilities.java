package com.utdallas.hope.tabParent;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.controller.FeaturesActivity;
import com.utdallas.hope.controller.UtilitiesThingsActivity;
import com.utdallas.hope.gridModel.LocatorThings;
import com.utdallas.hope.gridModel.UtilitiesThings;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.menuFunction.Search;

public class Utilities extends TabActivity  {
	/* Called when the activity is first created. */
	private TabHost mTabHost;
	
	UtilitiesThingsActivity uthingsActivityObject=new UtilitiesThingsActivity();
	
	TabSpec actionsTabSpec;
	TabSpec thingsTabSpec;
	

	TextView acc;
	
	public Context utilitiesContext = this;
	private static final int PREFERENCE = 12;

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data)
	    {
	    switch(requestCode) {
	    
	    case PREFERENCE:
	    	 	SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
	    }
	    }


private void showConfiguration_settings() {
	  		// TODO Auto-generated method stub
	  			Intent intent = new Intent().setClass(this,Preferences.class);
	  			
	  			startActivityForResult(intent,PREFERENCE);
	  		
	  	}


@Override
public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);
    return true;
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
    case R.id.configuration_settings:
    	showConfiguration_settings();
        return true;
    case R.id.help:
        showHelp();
        return true;
    case R.id.search:
        showSearch();
        return true;
    case R.id.bookmark:
    	addToFavorites();
        return true;
    default:
        return super.onOptionsItemSelected(item);
    }
}
	private void addToFavorites() {
	// TODO Auto-generated method stub
	
}

	private void showSearch() {
		SentenceGeneration.clearAllApplicationVariables();
		Intent intent = new Intent().setClass(this,Search.class);
		
		startActivity(intent);
	
}

	private void showHelp() {
	// TODO Auto-generated method stub
	
}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communication);
		
	
		 TextView tv= (TextView)findViewById(R.id.title);
			tv.setText(" Hope > Utilities");
		
       
		String extraParam=getIntent().getStringExtra("refresh");
		
		ImageButton homeButton =(ImageButton)findViewById(R.id.home);
		homeButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				startActivity(intent);
				
			}

			});
		LinearLayout topButtonLayout =(LinearLayout)findViewById(R.id.topButtonLayout);
		topButtonLayout.setVisibility(View.GONE);
		EditText acc =(EditText)findViewById(R.id.acc);
		acc.setVisibility(View.GONE);
		
		mTabHost = getTabHost();
		
		if(extraParam==null)
		{	
		
		
			Intent intent = new Intent().setClass(this, uthingsActivityObject.getClass());
			thingsTabSpec=mTabHost.newTabSpec("uthings").setIndicator("Utilities Things",
					getResources().getDrawable(R.drawable.ic_utilities))
			.setContent(intent);
			mTabHost.addTab(thingsTabSpec);
			mTabHost.setCurrentTab(0);
		
		}
		else
		{
			
			Intent intent = new Intent().setClass(this,UtilitiesThingsActivity.class);
			intent.putExtra("loadTable", "table");
			thingsTabSpec=mTabHost.newTabSpec("uthings").setIndicator("Utilities Things",
					getResources().getDrawable(R.drawable.ic_utilities))
			.setContent(intent);
					mTabHost.addTab(thingsTabSpec);
			mTabHost.setCurrentTab(0);	
			
			
		}
		
		//whatever happens set the features tab as constant 

		
		Intent intent = new Intent().setClass(this, FeaturesActivity.class);
		thingsTabSpec=mTabHost.newTabSpec("features").setIndicator("Features",
				getResources().getDrawable(R.drawable.ic_features))
		.setContent(intent);
				mTabHost.addTab(thingsTabSpec);
				
				mTabHost.setCurrentTab(0);
	
		
		
		mTabHost.setOnTabChangedListener(new OnTabChangeListener(){
			@Override
			public void onTabChanged(String tabId) {
				
				 TextView tv= (TextView)findViewById(R.id.title);
					if(mTabHost.getCurrentTab()==1){
					
						tv.setText("Features (Utilities Active)");
					}else{
						tv.setText("Utilities > Things");
					}
				
				
		        
			   //empty
			}});
		
		
		
	}

	@Override
	  public  boolean onKeyDown(int keyCode, KeyEvent event) {
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	// TODO Auto-generated method stub
	//super.onBackPressed();


	if (!HopeApplication.uthings.isEmpty()) {
		if (mTabHost.getCurrentTabTag().equalsIgnoreCase( "Utilities Things")) {
			if (HopeApplication.uthings.size() <= 1) {

			
				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
				thingsGrid.setAdapter(new UtilitiesThings(
						thingsGrid.getContext(),
						getLayoutInflater(), "uthings"));
			
				HopeApplication.uthings.remove(HopeApplication.uthings.get(HopeApplication.uthings.size() - 1));
			

			} else {

				
				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
				thingsGrid.setAdapter(new UtilitiesThings(
						thingsGrid.getContext(),
						getLayoutInflater(), HopeApplication.uthings
								.get(HopeApplication.uthings.size() - 1)));
			
				HopeApplication.uthings.remove(HopeApplication.uthings.get(HopeApplication.uthings.size() - 1));
				
			}

		}
	} else {
//		Toast.makeText(Emergencys.this, "Cannot go back",
//				Toast.LENGTH_SHORT).show();

	}

}
		return true;

	}
	
	
	

	
	
	
}
