package com.utdallas.hope.tabParent;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.app.TabActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.controller.ActionsActivity;
import com.utdallas.hope.controller.FeaturesActivity;
import com.utdallas.hope.controller.ThingsActivity;
import com.utdallas.hope.dialogDisplay.Last5Dialog;
import com.utdallas.hope.dialogDisplay.PronounDialog;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.menuFunction.Search;

public class SentenceGeneration extends TabActivity implements TextToSpeech.OnInitListener{
	/** Called when the activity is first created. */
	public static TabHost mTabHost;
	public ArrayList<String> things = new ArrayList<String>();
	public ArrayList<String> actions = new ArrayList<String>();
	ThingsActivity thingsActivityObject=new ThingsActivity();
	ActionsActivity actionsActivityObject= new ActionsActivity();
	TabSpec actionsTabSpec;
	TabSpec thingsTabSpec;
	private static final int PREFERENCE = 12;
	float currentAcceleration = 0;
	double calibration = 0;
	TextView acc;
	
	 TextToSpeech mTts;
	
public Context communicationContext=this;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communication);
		//Fill up the icon size preference
		SharedPreferences sizePreference = getSharedPreferences(
	               "sizePreference", Activity.MODE_PRIVATE);
	       HopeApplication.imgSize = sizePreference.getInt("iconSizePreference", 85);
	       HopeApplication.txtSize = sizePreference.getFloat("txtSizePreference",(float) 0.0);
		 mTts = new TextToSpeech(this,
		            this  
		            );

		String refresh=getIntent().getStringExtra("refresh");
		
		String fromSearch = getIntent().getStringExtra("fromSearch");
		String searchTable = getIntent().getStringExtra("searchTable");
		
		if(refresh==null)
		{	
			if(fromSearch!=null && !fromSearch.equals("") && fromSearch.equals("Things")){
				mTabHost = getTabHost();
				
				Intent intent = new Intent().setClass(this, thingsActivityObject.getClass());
				intent.putExtra("searchTable", searchTable);
				thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
						getResources().getDrawable(R.drawable.ic_items))
				.setContent(intent);
						mTabHost.addTab(thingsTabSpec);
						
						intent = new Intent().setClass(this, actionsActivityObject.getClass());
						actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
								getResources().getDrawable(R.drawable.ic_actions))
						.setContent(intent);
						
						mTabHost.addTab(actionsTabSpec);
						
						mTabHost.setCurrentTab(0);
						fromSearch="";
						
			}
			else if(fromSearch!=null && !fromSearch.equals("") && fromSearch.equals("Actions")){
				mTabHost = getTabHost();
				
				Intent intent = new Intent().setClass(this, thingsActivityObject.getClass());
				
				thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
						getResources().getDrawable(R.drawable.ic_items))
				.setContent(intent);
						mTabHost.addTab(thingsTabSpec);
						
						intent = new Intent().setClass(this, actionsActivityObject.getClass());
						intent.putExtra("searchTable", searchTable);
						actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
								getResources().getDrawable(R.drawable.ic_actions))
						.setContent(intent);
						
						mTabHost.addTab(actionsTabSpec);
						
						mTabHost.setCurrentTab(1);
						fromSearch="";
			}else{
				mTabHost = getTabHost();
		
				Intent intent = new Intent().setClass(this, thingsActivityObject.getClass());
			thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
					getResources().getDrawable(R.drawable.ic_items))
			.setContent(intent);
			mTabHost.addTab(thingsTabSpec);
		
			intent = new Intent().setClass(this, actionsActivityObject.getClass());
			actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
					getResources().getDrawable(R.drawable.ic_actions))
			.setContent(intent);
		
			mTabHost.addTab(actionsTabSpec);
		
			mTabHost.setCurrentTab(2);
			}
		
		}
		else
		{
			if(refresh.equals("actions"))
			{
			mTabHost = getTabHost();
			
			Intent intent = new Intent().setClass(this, ThingsActivity.class);
			intent.putExtra("loadTable", "table");
			thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
					getResources().getDrawable(R.drawable.ic_items))
			.setContent(intent);
					mTabHost.addTab(thingsTabSpec);
			
			intent = new Intent().setClass(this, actionsActivityObject.getClass());
			actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
					getResources().getDrawable(R.drawable.ic_actions))
			.setContent(intent);
			
			mTabHost.addTab(actionsTabSpec);
			
			mTabHost.setCurrentTab(1);	
			
			TextView tv= (TextView)findViewById(R.id.title);
			tv.setText("Sentence Generation > Actions");
			
			
			 TextView tv1=(TextView)findViewById(R.id.acc);
		 	 tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);

			}
			
			if(refresh.equalsIgnoreCase("commentsToThings"))
			{
				mTabHost = getTabHost();
				
				HopeApplication.things.clear();
				HopeApplication.things.add(HopeApplication.actions.get(1));
				Intent intent = new Intent().setClass(this, ThingsActivity.class);
				intent.putExtra("loadTable", "table");
				thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
						getResources().getDrawable(R.drawable.ic_items))
				.setContent(intent);
						mTabHost.addTab(thingsTabSpec);
						
						intent = new Intent().setClass(this, actionsActivityObject.getClass());
						actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
								getResources().getDrawable(R.drawable.ic_actions))
						.setContent(intent);
						
						mTabHost.addTab(actionsTabSpec);
						
						mTabHost.setCurrentTab(0);
						HopeApplication.actions.clear();HopeApplication.selectedActionTable="";
						
				
			}
			if(refresh.equalsIgnoreCase("verbsToThings"))
			{
				Toast.makeText(this, "verbs to things", Toast.LENGTH_SHORT).show();
				mTabHost = getTabHost();
				
				HopeApplication.things.clear();
				Intent intent = new Intent().setClass(this, thingsActivityObject.getClass());
				intent.putExtra("loadTable", "forVerbs");
				thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
						getResources().getDrawable(R.drawable.ic_items))
				.setContent(intent);
				
				mTabHost.addTab(thingsTabSpec);
				
				 intent = new Intent().setClass(this, ActionsActivity.class);
				intent.putExtra("loadTable", "table");
				actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
						getResources().getDrawable(R.drawable.ic_actions))
				.setContent(intent);
						mTabHost.addTab(actionsTabSpec);
						mTabHost.setCurrentTab(0);
				
			}
			if(refresh.equalsIgnoreCase("ThingsToVerbs"))
			{
				Toast.makeText(this, "things to verbs", Toast.LENGTH_SHORT).show();
				mTabHost = getTabHost();
				
				//HopeApplication.things.clear();
				Intent intent = new Intent().setClass(this, ThingsActivity.class);
				intent.putExtra("loadTable", "table");
				thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
						getResources().getDrawable(R.drawable.ic_items))
				.setContent(intent);
				
				mTabHost.addTab(thingsTabSpec);
				
				 intent = new Intent().setClass(this, ActionsActivity.class);
				intent.putExtra("loadTable", "forThings");
				actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
						getResources().getDrawable(R.drawable.ic_actions))
				.setContent(intent);
						mTabHost.addTab(actionsTabSpec);
						mTabHost.setCurrentTab(1);
				
			}
			
			if(refresh.equalsIgnoreCase("questionVerbToThings"))
			{
				mTabHost = getTabHost();
				
				HopeApplication.things.clear();
				//HopeApplication.things.add(HopeApplication.actions.get(1));
				Intent intent = new Intent().setClass(this, ThingsActivity.class);
				intent.putExtra("loadTable", "forQuestionVerb");
				thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
						getResources().getDrawable(R.drawable.ic_items))
				.setContent(intent);
						mTabHost.addTab(thingsTabSpec);
						
						intent = new Intent().setClass(this, actionsActivityObject.getClass());
						actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
								getResources().getDrawable(R.drawable.ic_actions))
						.setContent(intent);
						
						mTabHost.addTab(actionsTabSpec);
						
						mTabHost.setCurrentTab(0);
						HopeApplication.actions.clear();HopeApplication.selectedActionTable="";
						
				
			}
			
			
			if(refresh.equalsIgnoreCase("features"))
			{	
				
			mTabHost = getTabHost();
			
	Intent intent = new Intent().setClass(this, thingsActivityObject.getClass());
	thingsTabSpec=mTabHost.newTabSpec("things").setIndicator("Things",
			getResources().getDrawable(R.drawable.ic_items))
	.setContent(intent);
			mTabHost.addTab(thingsTabSpec);
			
			intent = new Intent().setClass(this, actionsActivityObject.getClass());
			actionsTabSpec=mTabHost.newTabSpec("actions").setIndicator("Actions",
					getResources().getDrawable(R.drawable.ic_actions))
			.setContent(intent);
			
			mTabHost.addTab(actionsTabSpec);
			
			mTabHost.setCurrentTab(0);
			
			TextView tv= (TextView)findViewById(R.id.title);
			tv.setText("Sentence Generation > Things");
			
			}

		}

		//whatever happens set the features tab as constant 

		Intent intent = new Intent().setClass(this, FeaturesActivity.class);
		
		thingsTabSpec=mTabHost.newTabSpec("features").setIndicator("Features",
				getResources().getDrawable(R.drawable.ic_features))
		.setContent(intent);
				mTabHost.addTab(thingsTabSpec);
		if(refresh==null && fromSearch==null)
		{
			
				mTabHost.setCurrentTab(2);
				
		}
		
		
		
		
		
		
		
		
		TextView sentenceText= (TextView)findViewById(R.id.acc);
		
		
		if(HopeApplication.actions.size()>0 )
		{
			if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))
			{
				HopeApplication.sentenceType="question";
			}
			else
			{
				HopeApplication.sentenceType="normal";
			}
		}
		if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
		{
			
			sentenceText.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
			
		}
		

		if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
		{
			
			sentenceText.setText( HopeApplication.question+" do "+HopeApplication.pronoun+" "+
					HopeApplication.verb+" "+ HopeApplication.object + " ? ");
			
			
		}
		
		
		
		
		mTabHost.setOnTabChangedListener(new OnTabChangeListener(){
			@Override
			public void onTabChanged(String tabId) {
				if(mTabHost.getCurrentTabTag().equals("things")) {
					TextView title= (TextView)findViewById(R.id.title);
					title.setText("Sentence Generation > Things");
					if(HopeApplication.actions.size()==1)
					{
						if(HopeApplication.actions.get(0).equalsIgnoreCase("verbs") && !HopeApplication.verb.equalsIgnoreCase(""))
						{
							if(HopeApplication.things.size()==0)
							{
							finish();
							HopeApplication.things.clear();
							Intent intent = new Intent().setClass(communicationContext,SentenceGeneration.class);
							intent.putExtra("refresh", "verbsToThings");
						
							startActivity(intent);
							//HopeApplication.things.clear();
							}
						}
					}
					else
					{
						
						if(HopeApplication.actions.size()==2)
						{
							if(HopeApplication.actions.get(0).equalsIgnoreCase("comments")&& HopeApplication.things.size()==0)
							{
								finish();
				    			Intent intent = new Intent().setClass(communicationContext,SentenceGeneration.class);
								intent.putExtra("refresh", "commentsToThings");
								HopeApplication.things.clear();
								
								startActivity(intent);
							}
							if(HopeApplication.actions.get(0).equalsIgnoreCase("questions")&& HopeApplication.things.size()==0 && !HopeApplication.verb.equalsIgnoreCase(""))
							{
								finish();
				    			Intent intent = new Intent().setClass(communicationContext,SentenceGeneration.class);
								intent.putExtra("refresh", "questionVerbToThings");
								HopeApplication.things.clear();
								
								startActivity(intent);
							}
						}
					}
				}
				 if(mTabHost.getCurrentTabTag().equals("actions")) {
					 TextView title= (TextView)findViewById(R.id.title);
						title.setText("Sentence Generation > Actions");
					 if(HopeApplication.things.size()>=1 )
					 {
						if(HopeApplication.actions.size()==1)
						{
							if(HopeApplication.actions.get(0).equalsIgnoreCase("verbs"))
							{
								finish();
								Intent intent = new Intent().setClass(communicationContext,SentenceGeneration.class);
								intent.putExtra("refresh", "ThingsToVerbs");
								//HopeApplication.things.clear();
								
								startActivity(intent);
							}
							
						}
						else
						{
							
						}	
					 }
					 else
					 {
						 if(HopeApplication.actions.size()==1)
						 {
							 if(HopeApplication.actions.get(0).equalsIgnoreCase("verbs"))
							 {

							 }
						 }
						 
					 }
				 }TextView tv= (TextView)findViewById(R.id.title);
					if(mTabHost.getCurrentTab()==2){
					
						tv.setText("Features (Sentence Generation Active)");
					}
	
			}});
		
		
		Button pronounButton =(Button)findViewById(R.id.Pronoun_Button);
		pronounButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

			
        		Intent intent = new Intent().setClass(communicationContext,PronounDialog.class);
			
				startActivityForResult(intent,1);
			}});
		
		Button clearButton =(Button)findViewById(R.id.Clear_Button);
		clearButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
			}

			});
		
		ImageButton homeButton =(ImageButton)findViewById(R.id.home);
		homeButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				startActivity(intent);
				
			}

			});
		
		Button speakButton =(Button)findViewById(R.id.Speak_Button);
		speakButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				
				try{
					boolean present=false; boolean modified=true;
				Cursor cur= HopeApplication.db.query("added_sentences", null, null, null, null, null, null);
				while(cur.moveToNext())
				{
					if(cur.getString(1).equalsIgnoreCase(HopeApplication.newSentenceToAdd))
					{
						present=true;
						//Toast.makeText(communicationContext, "already present", Toast.LENGTH_SHORT).show();
					}
				}
				
				//add code here after sentence gen is done
				if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
				{}
				if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
				{}
				
				
				if(HopeApplication.newSentenceToAdd.equalsIgnoreCase(HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object) || HopeApplication.newSentenceToAdd.equalsIgnoreCase(HopeApplication.question+" do "+HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object))
				{
					modified=false;
				}
				TextView senText=(TextView)findViewById(R.id.acc);
				if(modified && !present && !HopeApplication.newSentenceToAdd.equalsIgnoreCase("")&& !senText.getText().toString().equalsIgnoreCase(""))
				{
					
					ContentValues cv= new ContentValues();
					cv.put("sentence" , HopeApplication.newSentenceToAdd);
					Toast.makeText(communicationContext, "added"+HopeApplication.newSentenceToAdd, Toast.LENGTH_SHORT).show();
					HopeApplication.db.insert("added_sentences", null, cv);
					HopeApplication.newSentenceToAdd="";
					
				}
				
				}
				catch(Exception e){
					Toast.makeText(communicationContext, "added to new table", Toast.LENGTH_SHORT).show();
					
					if( !HopeApplication.newSentenceToAdd.equalsIgnoreCase(""))
					{
						
						ContentValues cv= new ContentValues();
						cv.put("sentence" , HopeApplication.newSentenceToAdd);
						Toast.makeText(communicationContext, "added"+HopeApplication.newSentenceToAdd, Toast.LENGTH_SHORT).show();
						HopeApplication.db.insert("added_sentences", null, cv);
						HopeApplication.newSentenceToAdd="";
						
					}
					
					
				}
				//finish();
				TextView t=(TextView)findViewById(R.id.acc);
				if(!t.getText().toString().equalsIgnoreCase("my sentence"))
					
				{
				 mTts.speak(t.getText().toString(),
				            TextToSpeech.QUEUE_FLUSH,  
				            null);
				if(!HopeApplication.recentSentences.contains(t.getText().toString()))
				{
				 if(HopeApplication.recentSentences.size()==5)
				 {
					 
					 
					 for(int i=0;i<4;i++)
					 {
						 HopeApplication.recentSentences.set(i, HopeApplication.recentSentences.get(i+1));
					 }
					 HopeApplication.recentSentences.remove(4);
					 HopeApplication.recentSentences.add(t.getText().toString());
					 
				 }
				 else
				 {
					 HopeApplication.recentSentences.add(t.getText().toString());
					 
				 }
				}
			}}});
		
		
		ImageButton last5Button =(ImageButton)findViewById(R.id.Last5_Button);
		last5Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				//finish();
        		Intent intent = new Intent().setClass(communicationContext,Last5Dialog.class);
			
				startActivityForResult(intent, 2);
			}});
		

	
	
		EditText sentc=(EditText)findViewById(R.id.acc);

		sentc.addTextChangedListener(new TextWatcher() {
		public void afterTextChanged (Editable theWatchedText) {
		HopeApplication.newSentenceToAdd= ((EditText)findViewById(R.id.acc) ) .getText().toString();
		
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
	
		}); //addTextChangedListene
		/*
		final Handler handler=new Handler();

		final Runnable r = new Runnable()
		{
		    public void run() 
		    {
		       Toast.makeText(communicationContext, "Currently you are at - Bakery Cafe. Here, you ate chicken panini" +
		        		" without artichokes ", Toast.LENGTH_LONG).show();
		        handler.postDelayed(this, 6000);
		    }
		};


		handler.postDelayed(r, 6000);
		*/

		
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    switch(requestCode) {
    case 1: 
	TextView sentenceText= (TextView)findViewById(R.id.acc);

		if(HopeApplication.actions.size()>0 )
		{
			if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))
			{
				HopeApplication.sentenceType="question";
			}
			else
			{
				HopeApplication.sentenceType="normal";
			}
		}
		if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
		{
			
			sentenceText.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
			
		}
		
		if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
		{
			sentenceText.setText( HopeApplication.question+" do "+HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object + " ? ");

		}
        break;  
    case PREFERENCE:
    	clearAllApplicationVariables();
		finish();
		Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
		intent.putExtra("refresh", "features");
		startActivity(intent);
    }
   
    }


@Override
public void onInit(int status) {
	
	// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
    if (status == TextToSpeech.SUCCESS) {
        
        int result = mTts.setLanguage(Locale.US);
        
        if (result == TextToSpeech.LANG_MISSING_DATA ||
            result == TextToSpeech.LANG_NOT_SUPPORTED) {
           
            
        } else {
        	
           
        }
    } else {
        // Initialization failed.
        //Log.e(TAG, "Could not initialize TextToSpeech.");
    	
    }} 
	
	
	
public static void clearAllApplicationVariables() {
	HopeApplication.actions.clear();
	HopeApplication.things.clear();
	HopeApplication.pronoun="I";
	HopeApplication.verb="";
	HopeApplication.question="";
	HopeApplication.object="";
	HopeApplication.sentenceType="normal";
	HopeApplication.currentDisplayedThings.clear();
	HopeApplication.currentDisplayedActions.clear();
	HopeApplication.newSentenceToAdd="";
	HopeApplication.kpthings.clear(); 
	HopeApplication.currentDisplayedKeyPhraseThings.clear();
	HopeApplication.selectedActionTable="";
	HopeApplication.selectedThingTable="";
	HopeApplication.ethings.clear();
	HopeApplication.currentDisplayedEmergencyThings.clear();
	HopeApplication.rootTable="";
	HopeApplication.searchThings=false;
}	
	
@Override
public void onDestroy() {
    // Don't forget to shutdown!
    if (mTts != null) {
        mTts.stop();
        mTts.shutdown();
    }

    super.onDestroy();
}



public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);
    return true;
}




@Override
public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
    case R.id.configuration_settings:
    	showConfiguration_settings();
    	
        return true;
    case R.id.help:
        showHelp();
        return true;
    case R.id.search:
        showSearch();
        return true;
    case R.id.bookmark:
    	addToFavorites();
        return true;
    default:
        return super.onOptionsItemSelected(item);
    }
}
	private void addToFavorites() {
		
		boolean present=false; 
		Cursor cur= HopeApplication.db.query("favorites", null, null, null, null, null, null);
		while(cur.moveToNext())
		{
			if(cur.getString(1).equalsIgnoreCase(((TextView)findViewById(R.id.acc)).getText().toString()))
			{
				present=true;
				break;
			}
		}
		
		if(!present){
			if(!HopeApplication.verb.equals("") || !HopeApplication.object.equals("") || !HopeApplication.question.equals(""))
			{
				
				if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
				{
					ContentValues cv= new ContentValues();
					cv.put("text" , HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
					HopeApplication.db.insert("favorites", null, cv);
				}
				
	
				if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
				{
					ContentValues cv= new ContentValues();
					cv.put("text" , HopeApplication.question+" do "+HopeApplication.verb+" "+ HopeApplication.object + " ? ");
					HopeApplication.db.insert("favorites", null, cv);
					
				}
			}
		}
	
}

	private void showSearch() {
		clearAllApplicationVariables();
		Intent intent = new Intent().setClass(this,Search.class);
		
		startActivity(intent);
		
	
}

	private void showHelp() {
	// TODO Auto-generated method stub
	
}

	private void showConfiguration_settings() {
	// TODO Auto-generated method stub
		Intent intent = new Intent().setClass(this,Preferences.class);
		
		startActivityForResult(intent,PREFERENCE);
	
}


	
}
