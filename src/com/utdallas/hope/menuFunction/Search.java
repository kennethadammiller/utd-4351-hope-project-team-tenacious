package com.utdallas.hope.menuFunction;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.tabParent.SentenceGeneration;
import com.utdallas.hope.tabParent.Emergency;
import com.utdallas.hope.tabParent.KeyPhrases;
import com.utdallas.hope.tabParent.Locator;
import com.utdallas.hope.tabParent.Utilities;


public class Search extends Activity implements TextToSpeech.OnInitListener{
	public static String SQL_GET_ALL_TABLES="";
	public static String SQL_GET_ALL_DATA="";
	public static String SQL_SEARCH_DATA="";
	public static String searchedCategory="";
	public static String imageName="";
	public static String imgName="";
	public static String tableType="";
	Button search,clear_text,listenSearch,visit;
	ImageButton speech_recognition,home,back;
	TextView imageText,imageDescription;
	ImageView image_searched;
	AutoCompleteTextView autoView;
	 TextToSpeech mTts;
	 private static final int VOICE_RECOGNITION_REQUEST_CODE=234;
	private ArrayList<String> tableNames= new ArrayList<String>();
	private ArrayList<String> allData = new ArrayList<String>();
	private ArrayList<String> thingTables= new ArrayList<String>();
	private ArrayList<String> actionTables= new ArrayList<String>();
	 
	 @Override
	 	     public void onCreate (Bundle savedInstanceState) {
		 		super.onCreate(savedInstanceState);
	            setContentView(R.layout.search);
	            autoView = (AutoCompleteTextView) findViewById(R.id.autosearch);
	            search = (Button)findViewById(R.id.search);
	            clear_text = (Button)findViewById(R.id.clear_text);
	            listenSearch = (Button)findViewById(R.id.listenSearch);
	            back = (ImageButton)findViewById(R.id.back);
	            home = (ImageButton)findViewById(R.id.home);
	            visit = (Button)findViewById(R.id.visit);
	            speech_recognition = (ImageButton)findViewById(R.id.speech_recognition);
	            imageText = (TextView)findViewById(R.id.imageText);
	            imageDescription = (TextView)findViewById(R.id.imageDescription);
	            image_searched = (ImageView)findViewById(R.id.image_searched);
	            
	            mTts = new TextToSpeech(this,this);
	            
	            retrieveTableNames();
	            
	            retrieveAllData();

	 	        fillAutoCompleteList();
	 	        
	 	        //Clear button code
	 	     
	 	       clear_text.setOnClickListener(new View.OnClickListener() {
	 				public void onClick(View view) {
	 					autoView.setText("");
	 					clearData();

	 					
	 				}});
	 	      
	 	      //Back button code
	 	   
	 	     back.setOnClickListener(new View.OnClickListener() {
	 				public void onClick(View view) {

	 					backButtonClicked();
	 	        		
	 				}});
	 	     
	 	     //home button code
	 	    home.setOnClickListener(new View.OnClickListener() {
 				public void onClick(View view) {

 					homeButtonClicked();
 	        		
 				}});
	 	    
	 	    search.setOnClickListener(new View.OnClickListener() {
	 				public void onClick(View view) {

	 					searchButtonClicked();
	 	        		
	 				}});
	 	    
	 	    //speech recognition button code
	 	   
	 	   speech_recognition.setOnClickListener(new View.OnClickListener() {
	 				public void onClick(View view) {

	 					startSpeechRecognition();
	 	        		
	 				}});
	 	   
	 	   // Listen - Text-to-Speech button clicked
	 	  listenSearch.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {

					startTextToSpeech();
	        		
				}});
	 	  //Visit the page containing the item searched
	 	 visit.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {

					visitPage();
	        		
				}});
   
	 	     }//end of onCreate method
	 
	 
	 void retrieveTableNames(){
		 Cursor cur=null;
		 SQL_GET_ALL_TABLES = "SELECT name FROM " +
         "sqlite_master WHERE type='table'";
         cur = HopeApplication.db.rawQuery(SQL_GET_ALL_TABLES, null);
         String names;
        while (cur.moveToNext()) {
	       	if(!(names=(String)cur.getString(0)).contains("_") &&
	       			!(names=(String)cur.getString(0)).equalsIgnoreCase("favorites")){
	       		tableNames.add(names);
	       	}
 	    }
        cur.close();
        cur.deactivate();
	 }
	 
	 void retrieveAllData(){
		 Cursor curData = null ;
		 for(int i=0;i<tableNames.size();i++){
			 SQL_GET_ALL_DATA = "SELECT name FROM " + tableNames.get(i);
	         curData = HopeApplication.db.rawQuery(SQL_GET_ALL_DATA, null);
	         String data;
	         while (curData.moveToNext()) {
	        	 data = curData.getString(0);
	        	 allData.add(data);
	 	     }
		 }
		 curData.close();
		 curData.deactivate();
	 }
	 
	 void fillAutoCompleteList(){
		 ArrayAdapter<String> searchAdapter = new ArrayAdapter<String>(this, R.layout.auto_list, allData);
          autoView.setAdapter(searchAdapter);
	 }
	 
	 void startSpeechRecognition(){
		 Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		 intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		 intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak Clearly Through Mic");   
		  startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);

	 }
	 
	 void searchButtonClicked(){
		 boolean found=false;
		 Cursor searchData=null;
		 clearData();
		 for(int i=0;i<tableNames.size();i++){
			 SQL_SEARCH_DATA = "SELECT name,icon,text FROM " + "'" + tableNames.get(i) + "'" +
			     " WHERE name like " + "'" + autoView.getText().toString() + "'";
			 searchData = HopeApplication.db.rawQuery(SQL_SEARCH_DATA, null);
	         while (searchData.moveToNext()) {
	        	 searchedCategory = tableNames.get(i);
	        	 imageName=searchData.getString(2);
	        	 imgName=searchData.getString(1);
	        	 found=true;
	        	 break;
	         }
   
		 }
		 searchData.close();
         searchData.deactivate();
		 displaySearchResults(found);
	 }
	 
	 void displaySearchResults(boolean hasFound){
		 if(hasFound){
			 
			 image_searched.setVisibility(image_searched.VISIBLE);
			 image_searched.setImageResource(this.getResources().getIdentifier(imgName,
					 "drawable","com.utdallas.hope"));
			 listenSearch.setVisibility(listenSearch.VISIBLE);
			 imageText.setVisibility(imageText.VISIBLE);
			 imageText.setText(imageName);
			 imageDescription.setVisibility(imageDescription.VISIBLE);
			 imageDescription.setText("The item " + autoView.getText().toString() +
					 " is under category: " + searchedCategory);
			 visit.setVisibility(visit.VISIBLE);
		 }else{
			 imageDescription.setVisibility(imageDescription.VISIBLE);
			 imageDescription.setText("Your searched item could not be found in the HOPE system !");
		 }
	 }
	 void startTextToSpeech(){
		 mTts.speak(imageName,TextToSpeech.QUEUE_FLUSH, null);
	 }
	 
	 void visitPage(){
		 Intent intent;
		 if(searchedCategory.equalsIgnoreCase("features")){
			  intent = new Intent().setClass(this, SentenceGeneration.class);
		 }
		 else if(searchedCategory.equalsIgnoreCase("keyphrases")){
			  intent = new Intent().setClass(this, KeyPhrases.class);
		 }
		 else if(searchedCategory.equalsIgnoreCase("greetings") ||
				 searchedCategory.equalsIgnoreCase("quickphrases") ){
			  intent = new Intent().setClass(this, KeyPhrases.class);
			  intent.putExtra("searchTable", searchedCategory);
		 }
		 else if(searchedCategory.equalsIgnoreCase("emergency")){
			  intent = new Intent().setClass(this, Emergency.class);
		 }
		 else if(searchedCategory.equalsIgnoreCase("locator")){
			  intent = new Intent().setClass(this, Locator.class);
		 }
		 
		 else if(searchedCategory.equalsIgnoreCase("utilities")){
			  intent = new Intent().setClass(this, Utilities.class);
		 }
		 else{
			  checkForThingActionTables();
			  String param="";
			  intent = new Intent().setClass(this, SentenceGeneration.class);
			  if(searchedCategory.equalsIgnoreCase("things")){
					param="Things";	
			  }
			  else if(searchedCategory.equalsIgnoreCase("actions")){
				 param="Actions";	
			  }
			  else if(tableType.equalsIgnoreCase("Things")){
				  param="Things";	
				  intent.putExtra("searchTable", searchedCategory);
			  }
			  else if(tableType.equalsIgnoreCase("Actions")){
				  param="Actions";	
				  intent.putExtra("searchTable", searchedCategory);
			  }
			 intent.putExtra("fromSearch",param);
			
		 }
		 	startActivity(intent);
			finish();
		 
	 }

	 
	 void checkForThingActionTables(){
		 //retrieve things table
		 Cursor curThings=null;
		String SQL_GET_THINGS = "SELECT name FROM 'things'";
		curThings = HopeApplication.db.rawQuery(SQL_GET_THINGS, null);
		while (curThings.moveToNext()) {
			thingTables.add(curThings.getString(0));
	     }
		curThings.close();
		curThings.deactivate();
		
		//retrieve actions table
		Cursor curActions=null;
		String SQL_GET_ACTIONS = "SELECT name FROM 'actions'";
		curActions = HopeApplication.db.rawQuery(SQL_GET_ACTIONS, null);
		while (curActions.moveToNext()) {
			actionTables.add(curActions.getString(0));
	     }
		curActions.close();
		curActions.deactivate();
		
		//Check the searched value in both lists and then check for children
		for(int i=0;i<actionTables.size();i++){
			if(searchedCategory.equalsIgnoreCase(actionTables.get(i))){
				tableType="Actions";
				break;
			}
		}
		if(!tableType.equals("Actions")){
			tableType="Things";
		}
	 }

	 void clearData(){
		 imageName="";
		 imgName="";
		 tableType="";
		 searchedCategory="";
		 image_searched.setVisibility(image_searched.GONE);
		 listenSearch.setVisibility(listenSearch.GONE);
		 imageText.setVisibility(imageText.GONE);
		 imageText.setText("");
		 imageDescription.setVisibility(imageDescription.GONE);
		 imageDescription.setText("");
		 visit.setVisibility(visit.GONE);
	 }
	 
	 void backButtonClicked(){
		 finish();
	 }
	 
	 void homeButtonClicked(){
		    SentenceGeneration.clearAllApplicationVariables();
			finish();
			Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
			startActivity(intent);
	 }

	@Override
	public void onInit(int status) {
		
		// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
	    if (status == TextToSpeech.SUCCESS) {
	        
	        int result = mTts.setLanguage(Locale.US);
	        
	        if (result == TextToSpeech.LANG_MISSING_DATA ||
	            result == TextToSpeech.LANG_NOT_SUPPORTED) {
	           
	            
	        } else {
	        	
	           
	        }
	    } else {
	        // Initialization failed.
	        //Log.e(TAG, "Could not initialize TextToSpeech.");
	    	
	    }} 
	 
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		if(VOICE_RECOGNITION_REQUEST_CODE==requestCode){
			if(data!=null){
				ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				if((String)matches.get(0)!=null && !((String)matches.get(0)).equals("") ){
					autoView.setText(matches.get(0));
				}
		
			}

		}
	}
	
	@Override
	public void onDestroy() {
	    // Don't forget to shutdown!
	    if (mTts != null) {
	        mTts.stop();
	        mTts.shutdown();
	    }

	    super.onDestroy();
	}
	 
}