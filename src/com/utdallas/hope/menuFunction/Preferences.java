package com.utdallas.hope.menuFunction;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.provider.Contacts.People;
import android.provider.Contacts.PeopleColumns;
import android.provider.Contacts.PhonesColumns;

import com.utdallas.hope.R;
 
public class Preferences extends PreferenceActivity {
	private static final int PICK_CONTACT = 2;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                addPreferencesFromResource(R.xml.preferences);
                // Get the custom preference for changing icon size
                Preference sizePref = findPreference("sizePref");
                sizePref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                    @Override
					public boolean onPreferenceClick(Preference preference) {
                    	callIconSizeChange();
                      
                         return true;
                       }
                 });
                // Get the custom preference for selecting emergency contact
                Preference eContactPref = findPreference("eContactPref");
                eContactPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                    @Override
					public boolean onPreferenceClick(Preference preference) {
                    	Intent intent = new Intent(Intent.ACTION_PICK, People.CONTENT_URI);
                    	startActivityForResult(intent, PICK_CONTACT);
                         return true;
                       }
                 });
        }
       
        void callIconSizeChange(){
        	Intent intent = new Intent().setClass(this,IconSizeChange.class);
            startActivityForResult(intent,23);
        }
        @Override
       	public void onActivityResult(int requestCode, int resultCode, Intent data) {
       		// TODO Auto-generated method stub
       		super.onActivityResult(requestCode, resultCode, data);
       	if (requestCode == PICK_CONTACT) {
       		 if (resultCode == Activity.RESULT_OK) {
       	        Uri contactData = data.getData();
       	        Cursor c =  managedQuery(contactData, null, null, null, null);
       	        if (c.moveToFirst()) {
       	          String name = c.getString(c.getColumnIndexOrThrow(PeopleColumns.NAME));
       	          String number = c.getString(c.getColumnIndexOrThrow(PhonesColumns.NUMBER));
       	          if(number !=null && number.length()>0 && number.contains("-")){
       	          number = number.replaceAll("-", "");
       	          }
       	          number = "tel:" + number;
       	          SharedPreferences eConPreference = getSharedPreferences(
                       "eConPreference", Context.MODE_PRIVATE);
          		SharedPreferences.Editor editor = eConPreference
                       .edit();
          		editor.putString("eConName", name);
          		editor.putString("eConNumber", number);    
          		editor.commit();
       	        }
       	      }
       		
       	 }
       	
       		
       	}
      
}
