package com.utdallas.hope.menuFunction;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.tabParent.SentenceGeneration;

public class IconSizeChange extends Activity {
	 float txtSize, newTxtSize ;
	 int imageParam=85, newImageParam = 85;
	Button save, cancel;
	ImageButton home,back;
	private static final int PREFERENCE = 12;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
		    {
		    switch(requestCode) {
		    
		    case PREFERENCE:
		    	Intent startAgain = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
		         startAgain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(startAgain);
		    }
		    }


	private void showConfiguration_settings() {
		  		// TODO Auto-generated method stub
		  			Intent intent = new Intent().setClass(this,Preferences.class);
		  			
		  			startActivityForResult(intent,PREFERENCE);
		  		
		  	}
	
   /** Called when the activity is first created. */
   @Override
   
   public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.iconsize);
      
       SeekBar seekBar = (SeekBar)findViewById(R.id.seekbar);
       
       ImageButton back = (ImageButton)findViewById(R.id.back);
       ImageButton home = (ImageButton)findViewById(R.id.home);
       final ImageView imageSample = (ImageView)findViewById(R.id.img_sample);
       final TextView imageText = (TextView)findViewById(R.id.txt_sample);
       SharedPreferences sizePreference = getSharedPreferences(
               "sizePreference", Context.MODE_PRIVATE);
       LayoutParams lp = imageSample.getLayoutParams();
       lp.height = lp.width  = sizePreference.getInt("iconSizePreference", imageParam);
       txtSize = sizePreference.getFloat("txtSizePreference", imageText.getTextSize());
       imageSample.setLayoutParams(lp);
       imageText.setTextSize(txtSize);
       int seekProgress = (lp.height*2) - 120;
       seekBar.setProgress(seekProgress);

       seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
       
   @Override
   public void onProgressChanged(SeekBar seekBar, int progress,
     boolean fromUser) {
   
	   imageParam=85;
	   newImageParam=85;//As 85 is the normal size of the imageView
	   //60 is the minimum icon height and 110 is the maximum in our app
	   newImageParam = 60 + (progress/2);
	   newTxtSize = (newImageParam * txtSize)/imageParam; 
	   imageText.setTextSize(newTxtSize);
	   LayoutParams lp = imageSample.getLayoutParams();
	   lp.height = lp.width = newImageParam;
       imageSample.setLayoutParams(lp);
   }

   @Override
   public void onStartTrackingTouch(SeekBar seekBar) {
    // TODO Auto-generated method stub
   }

   @Override
   public void onStopTrackingTouch(SeekBar seekBar) {
      
   }
       });
       
       save = (Button)findViewById(R.id.Save);
       cancel = (Button)findViewById(R.id.Cancel);
       
       save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				SharedPreferences sizePreference = getSharedPreferences(
                        "sizePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sizePreference
                        .edit();
        editor.putInt("iconSizePreference", newImageParam);
        editor.putFloat("txtSizePreference", newTxtSize);
                       
        editor.commit();
				finish();
			}
			
       });
       
       cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
			
      });
       
       back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				backButtonClicked();
        		
			}});
     
     //home button code
    home.setOnClickListener(new View.OnClickListener() {
		public void onClick(View view) {

			homeButtonClicked();
    		
		}});
       
   }
   
   void backButtonClicked(){
		 finish();
	 }
	 
	 void homeButtonClicked(){
		    SentenceGeneration.clearAllApplicationVariables();
			finish();
			Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
			startActivity(intent);
	 }
}