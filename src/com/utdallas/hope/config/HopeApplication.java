package com.utdallas.hope.config;

import java.util.ArrayList;

import com.utdallas.hope.database.InitializeDatabase;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
public class HopeApplication extends Application {
	public static InitializeDatabase dbHelper;
	public static SQLiteDatabase db;
	public static ArrayList<String> things = new ArrayList<String>();
	public static ArrayList<String> actions = new ArrayList<String>();
	public static ArrayList<String> currentDisplayedActions = new ArrayList<String>();
	public static ArrayList<String> currentDisplayedThings = new ArrayList<String>();
	
	public static ArrayList<String> recentSentences = new ArrayList<String>();
	
	public static int thingMovement=0;
	public static int actionMovement=0;
	public static int imgSize=85;
	public static float txtSize = (float)0.0;
	public static ArrayList<String> kpthings = new ArrayList<String>();
	public static ArrayList<String> currentDisplayedKeyPhraseThings = new ArrayList<String>();
	
	public static ArrayList<String> ethings = new ArrayList<String>();
	public static ArrayList<String> currentDisplayedEmergencyThings = new ArrayList<String>();
	
	public static ArrayList<String> lthings = new ArrayList<String>();
	public static ArrayList<String> currentDisplayedLocatorThings = new ArrayList<String>();
	
	public static ArrayList<String> uthings = new ArrayList<String>();
	public static ArrayList<String> currentDisplayedUtilitiesThings = new ArrayList<String>();
	
	
	public static String rootTable="";
	public static String sentenceType="normal";
	public static String pronoun="I";
	public static String verb="";
	public static String object="";
	public static String question="";
	public static String newSentenceToAdd="";
	
	public static String selectedActionTable="";
	public static String selectedThingTable="";
	
	public static boolean searchThings=false;

private static HopeApplication singleton;
// Returns the application instance
public static HopeApplication getInstance() {
return singleton;
}
@Override
public final void onCreate() {
super.onCreate();
singleton = this;
 dbHelper=new InitializeDatabase(this);
db=dbHelper.getWritableDatabase();
}
}

// use this to store and return value

// MyObject value = MyApplication.getInstance().getGlobalStateValue();
// MyApplication.getInstance().setGlobalStateValue(myObjectValue);