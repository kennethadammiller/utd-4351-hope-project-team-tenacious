package com.utdallas.hope.gridModel;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;

public class Features extends BaseAdapter {
    private Context mContext;
    private LayoutInflater li;
    int count=0;
    private Cursor mCursor;
    Bitmap bit, newBit;
    public Features(Context c,  LayoutInflater li) {
    	this.li=li;
        mContext = c;
        mCursor=HopeApplication.db.query("features", new String[]{"_id","name","icon","text"}, null, null, null,
		        null, null);
        
        while (mCursor.moveToNext()) {
        	
        	mThumbIds.add(mCursor.getString(2)); 
        	mThumbStr.add(mCursor.getString(3));
        	
  	    }
    }

    @Override
	public int getCount() {
        return mThumbIds.size();
    }

    @Override
	public Object getItem(int position) {
        return null;
    } 

    @Override
	public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
    	View v;
    	bit = null;
		newBit = null;
    	
        if(convertView==null){
             if(position==0){
            	 count++;
             }
             if(count==2){
            	 position=6;
            	 count=0;
             }
            //LayoutInflater li = getLayoutInflater();
        	 //Toast.makeText(mContext, " " +position, Toast.LENGTH_SHORT).show();
            v = li.inflate(R.layout.icontext, null);
             
            TextView tv = (TextView)v.findViewById(R.id.icon_text);
            ImageView iv = (ImageView)v.findViewById(R.id.icon_image);
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            iv.setPadding(8, 8, 8, 8);

            tv.setText(mThumbStr.get(position));
           
            if(HopeApplication.txtSize!=0.0){
           	 tv.setTextSize(HopeApplication.txtSize);
            }
          //  tv.setTextSize()
            LayoutParams lp = iv.getLayoutParams();
     	   lp.height = lp.width = HopeApplication.imgSize;
    
     	  iv.setLayoutParams(lp);
            
     	//  iv.setImageResource(mContext.getResources().getIdentifier(mThumbIds.get(position),"drawable","com.utdallas.hope"));
     	  
          bit = BitmapFactory.decodeResource(mContext.getResources(), 
        		  mContext.getResources().getIdentifier(mThumbIds.get(position),"drawable","com.utdallas.hope"));
         
  		if (bit != null) {
  			newBit = Bitmap.createScaledBitmap(bit, 90, 90, true);
  			bit.recycle();
  		}
  		if (newBit != null) {
  			iv.setImageBitmap(newBit);
  		}
           // Toast.makeText(mContext, mThumbStr.get(position), Toast.LENGTH_SHORT).show();
           
            
        }
        else
        {
            v = convertView;
        }
        return v;
    }

    // references to our images
    private ArrayList<String> mThumbIds= new ArrayList<String>();
	 private ArrayList<String> mThumbStr= new ArrayList<String>();
	 
}