package com.utdallas.hope.gridModel;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;

public class Things extends BaseAdapter{
	
	 private Context mContext;
	 private Cursor mCursor;
	 Bitmap bit, newBit;
private LayoutInflater li;
	    public Things(Context c, LayoutInflater li) {
	    	this.li=li;
	        mContext = c;
	        mCursor=HopeApplication.db.query("things", new String[]{"_id","name","icon","text"}, null, null, null,
			        null, null);
	        HopeApplication.currentDisplayedThings.clear();
	        while (mCursor.moveToNext()) {
	        	mThumbIds.add(mCursor.getString(2)); 
	        	mThumbStr.add(mCursor.getString(3));
	        	HopeApplication.currentDisplayedThings.add(mCursor.getString(1));
	  	    }
	        
	        
	    }

	    public Things(Context c, LayoutInflater li, Cursor cur, boolean selective) {
	    	this.li=li;
	        mContext = c;
	        //Toast.makeText(mContext,"hello world", Toast.LENGTH_SHORT).show();
	        try {
	    	if(!selective)
	    	{
	    	
	    		   mCursor=cur;
	    		   
	    		   HopeApplication.currentDisplayedThings.clear();
	   	        while (mCursor.moveToNext()) {
	   	        	mThumbIds.add(mCursor.getString(2)); 
	   	        	mThumbStr.add(mCursor.getString(3));
	   	        	HopeApplication.currentDisplayedThings.add(mCursor.getString(1));
	   	  	    }	
				
	   	     //HopeApplication.actions.add(actionText);
			
			
	    	}
	    	else
	    	{
	    		
	    		mCursor=cur;
	    	ArrayList<String> tables=new ArrayList<String>();
	    	//Toast.makeText(mContext, "got it"+mCursor.getCount(), Toast.LENGTH_SHORT).show();
	    	//Toast.makeText(mContext, "here"+mCursor.getColumnCount(), Toast.LENGTH_SHORT).show();
	    	while(mCursor.moveToNext())
	    	{
	    		tables.add(mCursor.getString(2).toLowerCase());
	    		
	    	}
	    	//Toast.makeText(mContext, "here", Toast.LENGTH_SHORT).show();
	    	mCursor.moveToFirst();
	    	
	    		Cursor c21=HopeApplication.db.query("things",null , null, null, null, null, null);// new String[]{"id","name","icon","text"}
	    		HopeApplication.currentDisplayedThings.clear();
	    		//Toast.makeText(mContext, "got it"+c21.getCount(), Toast.LENGTH_SHORT).show();
	    		mThumbIds.clear();
	    		mThumbStr.clear();
	   	        while (c21.moveToNext()) {
	   	        //	Toast.makeText(mContext, ""+c21.getString(1)+" "+tables.get(0), Toast.LENGTH_SHORT).show();
	   	        	if(tables.contains((c21.getString(1).toLowerCase()))){
	   	        		
	   	        	mThumbIds.add(c21.getString(2)); 
	   	        	mThumbStr.add(c21.getString(3));
	   	        	HopeApplication.currentDisplayedThings.add(c21.getString(1));
	   	        	}
	   	        	
	   	  	    }	
	   	     //Toast.makeText(mContext, mThumbIds.size(), Toast.LENGTH_SHORT).show();
	    		
	    	}
			
	    
	    
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			Toast.makeText(mContext, "excep"+e.getMessage(), Toast.LENGTH_SHORT).show();
		}
}
	    
	    
	    public Things(Context c, LayoutInflater li, String table) {
	    	this.li=li;
	        mContext = c;
	       
	        
	    	//Toast.makeText(mContext, "new constructor", Toast.LENGTH_SHORT).show();
	    	
	    	try {
	    		 mCursor=HopeApplication.db.query(table,null , null, null, null, null, null);
	    		 HopeApplication.currentDisplayedThings.clear();
	   	        while (mCursor.moveToNext()) {
	   	        	mThumbIds.add(mCursor.getString(2)); 
	   	        	mThumbStr.add(mCursor.getString(3));
	   	        	
	   	        	HopeApplication.currentDisplayedThings.add(mCursor.getString(1));
	   	        	
	   	        	
	   	        	
	   	  	    }	
					  
	   	        
	   	        
	   
	   	        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Toast.makeText(mContext, "excep"+e.getMessage(), Toast.LENGTH_SHORT).show();
			}
			
			
	    }
	    
	    
	    
	  

		@Override
		public int getCount() {
	        return mThumbIds.size();
	    }

	    @Override
		public Object getItem(int position) {
	        return null;
	        
	    }

	    @Override
		public long getItemId(int position) {
	        return 0;
	    }

	@Override
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
		View v;
		bit = null;
		newBit = null;
		
		            if(convertView==null){
		
		                //LayoutInflater li = getLayoutInflater();
		
		            	 v = li.inflate(R.layout.icontext, null);
		                 
		                 TextView tv = (TextView)v.findViewById(R.id.icon_text);
		                 ImageView iv = (ImageView)v.findViewById(R.id.icon_image);
		                 iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
		                 iv.setPadding(8, 8, 8, 8);

		                 tv.setText(mThumbStr.get(position));
		                 if(HopeApplication.txtSize!=0.0){
		                	 tv.setTextSize(HopeApplication.txtSize);
		                 }
		               //  tv.setTextSize()
		                 LayoutParams lp = iv.getLayoutParams();
		          	   lp.height = lp.width = HopeApplication.imgSize;
		         
		          	  iv.setLayoutParams(lp);
		               
		          	 //  iv.setImageResource(mContext.getResources().getIdentifier(mThumbIds.get(position),"drawable","com.utdallas.hope"));
		          	bit = BitmapFactory.decodeResource(mContext.getResources(), 
		          		  mContext.getResources().getIdentifier(mThumbIds.get(position),"drawable","com.utdallas.hope"));
		          	if (bit != null) {
		        			newBit = Bitmap.createScaledBitmap(bit, 90, 90, true);
		        			bit.recycle();
		        		}
		        		if (newBit != null) {
		        			iv.setImageBitmap(newBit);
		        		}
		
		 
		
		            }
		
		            else
		
		            {
		
		                v = convertView;
		
		            }
		
		            return v;


                
    }
	
	   // references to our images
	 private ArrayList<String> mThumbIds= new ArrayList<String>();
	 private ArrayList<String> mThumbStr= new ArrayList<String>();

}
