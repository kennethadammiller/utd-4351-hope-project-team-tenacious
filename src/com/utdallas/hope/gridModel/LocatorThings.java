package com.utdallas.hope.gridModel;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;

public class LocatorThings extends BaseAdapter{
	
	 private Context mContext;
	 private Cursor mCursor;
	 Bitmap bit, newBit;
private LayoutInflater li;
	    public LocatorThings(Context c, LayoutInflater li) {
	    	HopeApplication.lthings.clear();
	    	this.li=li;
	        mContext = c;
	        mCursor=HopeApplication.db.query("locator", new String[]{"_id","name","icon","text"}, null, null, null,
			        null, null);
	        HopeApplication.currentDisplayedLocatorThings.clear();
	        while (mCursor.moveToNext()) {
	        	mThumbIds.add(mCursor.getString(2)); 
	        	mThumbStr.add(mCursor.getString(3));
	        
	        	HopeApplication.currentDisplayedLocatorThings.add(mCursor.getString(1));
	  	    }
	        
	        
	    }

	    public LocatorThings(Context c, LayoutInflater li, Cursor cur, boolean selective, int position) {
	    	this.li=li;
	        mContext = c;
	       
	    	try {
	    		 mCursor=HopeApplication.db.query(HopeApplication.currentDisplayedLocatorThings.get(position),null , null, null, null, null, null);
	    		 HopeApplication.currentDisplayedLocatorThings.clear();
	   	        while (mCursor.moveToNext()) {
	   	        	mThumbIds.add(mCursor.getString(2)); 
	   	        	mThumbStr.add(mCursor.getString(3));
	   	        	
	   	        	HopeApplication.currentDisplayedLocatorThings.add(mCursor.getString(1));
   	        	
	   	  	    }	

	   	        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Toast.makeText(mContext, "excep"+e.getMessage(), Toast.LENGTH_SHORT).show();
			}
			
			
	    }
	    
	    public LocatorThings(Context c, LayoutInflater li, String table) {
	    	this.li=li;
	        mContext = c;
	    	try {
	    		 mCursor=HopeApplication.db.query(table,null , null, null, null, null, null);
	    		 HopeApplication.currentDisplayedLocatorThings.clear();
	   	        while (mCursor.moveToNext()) {
	   	        	mThumbIds.add(mCursor.getString(2)); 
	   	        	mThumbStr.add(mCursor.getString(3));
	   	        	
	   	        	HopeApplication.currentDisplayedLocatorThings.add(mCursor.getString(1));
	   	        	
	   	  	    }	
			    
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Toast.makeText(mContext, "excep"+e.getMessage(), Toast.LENGTH_SHORT).show();
			}
			
	    }
	    
		@Override
		public int getCount() {
	        return mThumbIds.size();
	    }

	    @Override
		public Object getItem(int position) {
	        return null;
	        
	    }

	    @Override
		public long getItemId(int position) {
	        return 0;
	    }

	@Override
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
		View v;
		bit = null;
		newBit = null;
		            if(convertView==null){
		
		            	 v = li.inflate(R.layout.icontext, null);
		            
		                
		                 TextView tv = (TextView)v.findViewById(R.id.icon_text);
		                 ImageView iv = (ImageView)v.findViewById(R.id.icon_image);
		                 iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
		                 iv.setPadding(8, 8, 8, 8);

		                 tv.setText(mThumbStr.get(position));
		                
		                 if(HopeApplication.txtSize!=0.0){
		                	 tv.setTextSize(HopeApplication.txtSize);
		                 }
		               //  tv.setTextSize()
		                 LayoutParams lp = iv.getLayoutParams();
		          	   lp.height = lp.width = HopeApplication.imgSize;
		         
		          	  iv.setLayoutParams(lp);
		          	
		          	bit = BitmapFactory.decodeResource(mContext.getResources(), 
		          		  mContext.getResources().getIdentifier(mThumbIds.get(position),"drawable","com.utdallas.hope"));
		        		if (bit != null) {
		        			newBit = Bitmap.createScaledBitmap(bit, 90, 90, true);
		        			bit.recycle();
		        		}
		        		if (newBit != null) {
		        			iv.setImageBitmap(newBit);
		        		}
		            }
		            else
		            {
		                v = convertView;
		            }
		            return v;
         
    }
	
	   // references to our images
	 private ArrayList<String> mThumbIds= new ArrayList<String>();
	 private ArrayList<String> mThumbStr= new ArrayList<String>();


}
