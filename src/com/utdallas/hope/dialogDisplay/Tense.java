package com.utdallas.hope.dialogDisplay;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;



public class Tense extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tense);
        
       Cursor c=HopeApplication.db.rawQuery("select present,past,future from verbs where text='"+HopeApplication.verb+"'", null);
        c.moveToFirst();
        TextView tv= (TextView)findViewById(R.id.present);
        tv.setText(c.getString(0));
        tv.setBackgroundColor(android.graphics.Color.WHITE);
		tv.setTextSize(20);
		
		tv.setHeight(50);
		tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	TextView t=(TextView)view;
            	HopeApplication.verb=t.getText().toString();
            	Toast.makeText(view.getContext(), HopeApplication.verb, Toast.LENGTH_SHORT).show();
            	
            setResult(1);
            	finish();
            	
            }
	}
	);
		
tv= (TextView)findViewById(R.id.past);
tv.setText(c.getString(1));
tv.setBackgroundColor(android.graphics.Color.WHITE);
tv.setTextSize(20);

tv.setHeight(50);
tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	TextView t=(TextView)view;
            	HopeApplication.verb=t.getText().toString();
            	Toast.makeText(view.getContext(), HopeApplication.verb, Toast.LENGTH_SHORT).show();
            	
            setResult(1);
            	finish();
            	
            }
	}
	);
		
		
tv= (TextView)findViewById(R.id.future);
tv.setText(c.getString(2));
tv.setBackgroundColor(android.graphics.Color.WHITE);
tv.setTextSize(20);

tv.setHeight(50);
tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	TextView t=(TextView)view;
            	HopeApplication.verb=t.getText().toString();
            	Toast.makeText(view.getContext(), HopeApplication.verb, Toast.LENGTH_SHORT).show();
            	
            setResult(1);
            	finish();
            	
            }
	}
	);	
		
		 
		
		
		
		
			
		
		
		
}}


