package com.utdallas.hope.dialogDisplay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.tabParent.SentenceGeneration;



public class PronounDialog extends Activity {
	private static final int PREFERENCE = 12;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
		    {
		    switch(requestCode) {
		    
		    case PREFERENCE:
		    	Intent startAgain = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
		         startAgain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(startAgain);
		    }
		    }


	private void showConfiguration_settings() {
		  		// TODO Auto-generated method stub
		  			Intent intent = new Intent().setClass(this,Preferences.class);
		  			
		  			startActivityForResult(intent,PREFERENCE);
		  		
		  	}
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pronoundialog);
        LinearLayout layout=(LinearLayout)this.findViewById(R.id.pronounLayout);
        layout.setBackgroundColor(android.graphics.Color.WHITE);
        
        TextView tv= new TextView(this);
		tv.setText("I");
		tv.setWidth(100);
		tv.setHeight(50);
		tv.setBackgroundColor(android.graphics.Color.WHITE);
		tv.setTextSize(20);
		tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	TextView t=(TextView)view;
            	HopeApplication.pronoun=t.getText().toString();
            	Toast.makeText(view.getContext(), HopeApplication.pronoun, Toast.LENGTH_SHORT).show();
            	
            setResult(1);
            	finish();
            	
            }
	}
	);
		layout.addView(tv);
		
		
		
		  tv= new TextView(this);
			tv.setText("We");
			tv.setWidth(100);
			tv.setHeight(50);
			tv.setBackgroundColor(android.graphics.Color.WHITE);
			tv.setTextSize(20);
			tv.setTextColor(android.graphics.Color.BLACK);
			
			tv.setOnClickListener(new View.OnClickListener() {
	            @Override
				public void onClick(View view) {
	            	TextView t=(TextView)view;
	            	HopeApplication.pronoun=t.getText().toString();
	            	Toast.makeText(view.getContext(), HopeApplication.pronoun, Toast.LENGTH_SHORT).show();
	            	
	            setResult(1);
	            	finish();
	            	
	            }
		}
		);
			layout.addView(tv);
		
		
		
		
		
			 tv= new TextView(this);
				tv.setText("You");
				tv.setWidth(100);
				tv.setHeight(50);
				tv.setBackgroundColor(android.graphics.Color.WHITE);
				tv.setTextSize(20);
				tv.setTextColor(android.graphics.Color.BLACK);
				
				
				tv.setOnClickListener(new View.OnClickListener() {
		            @Override
					public void onClick(View view) {
		            	TextView t=(TextView)view;
		            	HopeApplication.pronoun=t.getText().toString();
		            	Toast.makeText(view.getContext(), HopeApplication.pronoun, Toast.LENGTH_SHORT).show();
		            	
		            setResult(1);
		            	finish();
		            	
		            }
			}
			);
				layout.addView(tv);	
		
		
		
}}
