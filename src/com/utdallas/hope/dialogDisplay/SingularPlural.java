package com.utdallas.hope.dialogDisplay;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;

public class SingularPlural extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        setContentView(R.layout.singular_plural);
        Cursor c = HopeApplication.db.rawQuery("select text,plural from " + HopeApplication.rootTable + " where text='"+HopeApplication.object+"'", null);
        c.moveToFirst();
        
        TextView tv= (TextView)findViewById(R.id.singular);
        
        tv.setText(c.getString(0));
        tv.setBackgroundColor(android.graphics.Color.WHITE);
		tv.setTextSize(20);
		
		tv.setHeight(50);
		tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	TextView t=(TextView)view;
            	HopeApplication.object=t.getText().toString();
            	//Toast.makeText(view.getContext(), HopeApplication.object, Toast.LENGTH_SHORT).show();
            	
            setResult(1);
            	finish();
            	
            }
	}
	);
	
		
tv= (TextView)findViewById(R.id.plural);
tv.setText(c.getString(1));
tv.setBackgroundColor(android.graphics.Color.WHITE);
tv.setTextSize(20);

tv.setHeight(50);
tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	TextView t=(TextView)view;
            	HopeApplication.object=t.getText().toString();
            	//Toast.makeText(view.getContext(), HopeApplication.object, Toast.LENGTH_SHORT).show();
            	
            setResult(1);
            	finish();
            	
            }
	}
	);

}}

