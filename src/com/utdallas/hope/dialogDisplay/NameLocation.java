package com.utdallas.hope.dialogDisplay;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;

public class NameLocation extends Activity {
	TextView currentAddress;
	TextView locHeader;
	EditText enterLoc;
	LinearLayout nameLocLayout;
	LinearLayout buttonNameLoc;
	Button saveLocName;
	Button cancelLocName;
	Button ok;
	public static String myLocationName="";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.namelocation);
        String currAdd=getIntent().getStringExtra("currentAddress");
        currentAddress = (TextView)findViewById(R.id.currentAddress);
        locHeader = (TextView)findViewById(R.id.locHeader);
        nameLocLayout = (LinearLayout)findViewById(R.id.nameLocLayout);
        buttonNameLoc = (LinearLayout)findViewById(R.id.buttonNameLoc);
        saveLocName = (Button)findViewById(R.id.saveLocName);
        cancelLocName = (Button)findViewById(R.id.cancelLocName);
        ok = (Button)findViewById(R.id.ok);
        enterLoc = (EditText)findViewById(R.id.enterLoc);
        
        if(currAdd!=null && !currAdd.equals("")){
        	currentAddress.setText("Give a name for this location : ");
        	currentAddress.append(currAdd);
        }else{
        	currentAddress.setText("Current Location is already saved");
        	nameLocLayout.setVisibility(nameLocLayout.GONE);
        	buttonNameLoc.setVisibility(buttonNameLoc.GONE);
        	ok.setVisibility(ok.VISIBLE);
        	
        }
        
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	finish();
            	
            }
        });
        saveLocName.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	if(enterLoc.getText().toString()!=null && 
            			!enterLoc.getText().toString().equals("")){
            		myLocationName = enterLoc.getText().toString();
            		setResult(1);
                	finish();
            	}
            	else{
            		Toast.makeText(getBaseContext(), "Please enter name of the location", 
            				Toast.LENGTH_SHORT).show();
            	}
            	
            }
        });
        cancelLocName.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	setResult(-1);
            	finish();
            	
            }
        }
	);
	
}}


