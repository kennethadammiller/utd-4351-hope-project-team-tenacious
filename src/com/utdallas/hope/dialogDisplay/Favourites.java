package com.utdallas.hope.dialogDisplay;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.tabParent.SentenceGeneration;
public class Favourites extends Activity implements TextToSpeech.OnInitListener{
	
	TextToSpeech mTts;
	private static final int PREFERENCE = 12;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
		    {
		    switch(requestCode) {
		    
		    case PREFERENCE:
		    	Intent startAgain = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
		         startAgain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(startAgain);
		    }
		    }


	private void showConfiguration_settings() {
		  		// TODO Auto-generated method stub
		  			Intent intent = new Intent().setClass(this,Preferences.class);
		  			
		  			startActivityForResult(intent,PREFERENCE);
		  		
		  	}
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourites);
        LinearLayout layout=(LinearLayout)this.findViewById(R.id.fav);
        layout.setBackgroundColor(android.graphics.Color.WHITE);
        
        mTts = new TextToSpeech(this,
                this  // TextToSpeech.OnInitListener
                );
        Cursor cur=null;
        try {
			cur= HopeApplication.db.rawQuery("select text from favorites ", null);
		} catch (Exception e) {
			
		}
       
		while (cur.moveToNext()) 
        {
        TextView tv= new TextView(this);
		tv.setText(cur.getString(0));
		 //Toast.makeText(this, "done "+cur.getCount(), Toast.LENGTH_SHORT).show();
		
		tv.setHeight(50);
		tv.setBackgroundColor(android.graphics.Color.WHITE);
		tv.setTextSize(20);
		tv.setTextColor(android.graphics.Color.BLACK);
		tv.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            
            	TextView t=(TextView)view;
				 mTts.speak(t.getText().toString(),
				            TextToSpeech.QUEUE_FLUSH,  
				            null);
				 
				 finish();
            }
	}
	);
		layout.addView(tv);
		
        }
		
		
		
}

	@Override
	public void onInit(int status) {
		
		 // status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
        if (status == TextToSpeech.SUCCESS) {
            // Set preferred language to US english.
            // Note that a language may not be available, and the result will indicate this.
            int result = mTts.setLanguage(Locale.US);
        
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED) {
              
            } else {
               
               
            }
        } else {
            // Initialization failed.
           
        }
		
		
	}}
