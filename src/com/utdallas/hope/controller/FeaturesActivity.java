package com.utdallas.hope.controller;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.gridModel.Features;
import com.utdallas.hope.tabParent.SentenceGeneration;
import com.utdallas.hope.tabParent.Emergency;
import com.utdallas.hope.tabParent.KeyPhrases;
import com.utdallas.hope.tabParent.Locator;
import com.utdallas.hope.tabParent.Utilities;
public class FeaturesActivity extends Activity implements TextToSpeech.OnInitListener{
	public Context featuresContext=this;
	public TextToSpeech mTts;
	GridView gridview;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		 try {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.featuresactivity);
		 
		 mTts = new TextToSpeech(this,
		            this  // TextToSpeech.OnInitListener
		            );
			gridview = (GridView) findViewById(R.id.FeaturesGrid);
			//gridview.setNumColumns(2);
			   gridview.setAdapter(new Features(
					gridview.getContext(),getLayoutInflater()));
			   
			   gridview.setOnItemClickListener(new OnItemClickListener() {
			        @Override
					public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			       
			        	TextView tv = (TextView) v.findViewById(R.id.icon_text);
			        	
			        	mTts.speak(tv.getText().toString(),
			                    TextToSpeech.QUEUE_FLUSH,  // Drop all pending entries in the playback queue.
			                    null);
			        	Toast.makeText(v.getContext(), tv.getText().toString(), Toast.LENGTH_SHORT).show();
			        	if(tv.getText().toString().equalsIgnoreCase("sentence"))
			        	{
			        		getParent().finish();
			        		Intent intent = new Intent().setClass(featuresContext,SentenceGeneration.class);
							intent.putExtra("refresh", "features");
							startActivity(intent);
			        	}
			        	if(tv.getText().toString().equalsIgnoreCase("emergency"))
			        	{
			        		getParent().finish();
			        		Intent intent = new Intent().setClass(featuresContext,Emergency.class);
							startActivity(intent);
			        	}
			        	if(tv.getText().toString().equalsIgnoreCase("key phrases"))
			        	{
			        		getParent().finish();
			        		Intent intent = new Intent().setClass(featuresContext,KeyPhrases.class);
							startActivity(intent);
			        	}
			        	if(tv.getText().toString().equalsIgnoreCase("locator"))
			        	{
			        		getParent().finish();
			        		Intent intent = new Intent().setClass(featuresContext,Locator.class);
							startActivity(intent);
			        		
			        	}
			          	if(tv.getText().toString().equalsIgnoreCase("About Me"))
			        	{
			        		
			        		Intent intent = new Intent().setClass(featuresContext,AboutMe.class);
							startActivity(intent);
			        		
			        	}
			          	if(tv.getText().toString().equalsIgnoreCase("Utilities"))
			        	{
			        		
			        		getParent().finish();
			        		Intent intent = new Intent().setClass(featuresContext,Utilities.class);
							startActivity(intent);
							
			        		
			        	}
			        	
			        }
			    });
				
			   
			   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this, "exception", Toast.LENGTH_SHORT).show();
		}
	       
	      
	   
		
	}
	@Override
	public void onInit(int status) {
		
		
		
		// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
        if (status == TextToSpeech.SUCCESS) {
            // Set preferred language to US english.
            // Note that a language may not be available, and the result will indicate this.
            int result = mTts.setLanguage(Locale.US);
            
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED) {
               
            } else {
               
            }
        } else {
          
        }
		
	}

	  @Override
	    public void onDestroy() {
	        // Don't forget to shutdown!
	        if (mTts != null) {
	            mTts.stop();
	            mTts.shutdown();
	        }

	        super.onDestroy();
	        final GridView grid = gridview;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }
	    }
	
}

