package com.utdallas.hope.controller;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.menuFunction.Search;
import com.utdallas.hope.tabParent.SentenceGeneration;

public class AboutMe extends Activity implements TextToSpeech.OnInitListener{
	public Context aboutmeContext=this;
	public TextToSpeech mTts;
	private static final int PREFERENCE = 12;
	TextView personalInfo;
	TextView emergencyInfo;
	TextView medicalInfo;
	TextView nameHeader;
	TextView name;
	TextView ageHeader;
	TextView age;
	TextView addressHeader;
	TextView address;
	TextView cnameHeader;
	TextView cname;
	TextView cphoneHeader;
	TextView cphone;
	TextView bloodtypeHeader;
	TextView bloodtype;
	ImageView img;
	byte[] hash;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
		    {
		
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 3){

			displayInfoOnScreen();
			
		}
		   if( requestCode == PREFERENCE){
			   SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
		    }
		    }


	private void showConfiguration_settings() {
		  		// TODO Auto-generated method stub
		  			Intent intent = new Intent().setClass(this,Preferences.class);
		  			
		  			startActivityForResult(intent,PREFERENCE);
		  		
		  	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutme);
		 
		 mTts = new TextToSpeech(this,
		            this  // TextToSpeech.OnInitListener
		            );
			 
 personalInfo=(TextView)findViewById(R.id.personalInfo);
 nameHeader=(TextView)findViewById(R.id.nameHeader);
 name=(TextView)findViewById(R.id.name);
 
 ageHeader=(TextView)findViewById(R.id.ageHeader);
 age=(TextView)findViewById(R.id.age);
 
 addressHeader=(TextView)findViewById(R.id.addressHeader);
 address=(TextView)findViewById(R.id.address);
 
 emergencyInfo=(TextView)findViewById(R.id.emergencyInfo);
 cnameHeader=(TextView)findViewById(R.id.contactNameHeader);
 cname=(TextView)findViewById(R.id.contactName);
 
 cphoneHeader=(TextView)findViewById(R.id.phoneHeader);
 cphone=(TextView)findViewById(R.id.phone);
 
 medicalInfo=(TextView)findViewById(R.id.medicalInfo);
 bloodtypeHeader=(TextView)findViewById(R.id.bloodTypeHeader);
 bloodtype=(TextView)findViewById(R.id.bloodType);
 
 img=(ImageView)findViewById(R.id.addImage);

  Button update =(Button)findViewById(R.id.add_update1);
  
  Button speak =(Button)findViewById(R.id.speak1);
  
  ImageButton back =(ImageButton)findViewById(R.id.back);
  
  ImageButton home =(ImageButton)findViewById(R.id.home);
  //Display the stored information on screen
  displayInfoOnScreen();

speak.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {
         //Add functionality of reading out all the text in the page
		String pause = "  ";
		String about_Me = " Information about me   " + personalInfo.getText().toString() + pause +
		 nameHeader.getText().toString()+ pause +
		 name.getText().toString()+ pause +
		 ageHeader.getText().toString()+ pause +
		 age.getText().toString()+ pause +
		 addressHeader.getText().toString()+ pause +
		 address.getText().toString()+ pause +
		 emergencyInfo.getText().toString()+ pause +
		 cnameHeader.getText().toString()+ pause +
		 cname.getText().toString()+ pause +
		 cphoneHeader.getText().toString()+ pause +
		 cphone.getText().toString()+ pause +
		 medicalInfo.getText().toString()+ pause +
		 bloodtypeHeader.getText().toString()+ pause +
		 bloodtype.getText().toString();
		about_Me.replaceAll(":", " ");
		mTts.setSpeechRate(-15);
		mTts.speak(about_Me,
	            TextToSpeech.QUEUE_FLUSH,  
	            null);
		
	}});

update.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {

		//finish();
		Intent intent = new Intent().setClass(aboutmeContext,AboutMeEdit.class);
	
		startActivityForResult(intent, 3);
	}});

back.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {

		finish();
		
	}});

home.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {

		SentenceGeneration.clearAllApplicationVariables();
		finish();
		Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
		startActivity(intent);
		
	}});

	}
	
	void displayInfoOnScreen(){
		 try {
				Cursor personal= HopeApplication.db.query("personal_info", null, null, null, null, null, null);
				 while (personal.moveToNext()) {
					 if(personal.getString(0)!=null){
						 name.setText(personal.getString(0));
					 }
					 if(personal.getString(1)!=null){
						 age.setText(personal.getString(1));
					 }
					 if(personal.getString(2)!=null){
						 address.setText(personal.getString(2));
					 }
					//Add code for image also 
					 if(personal.getBlob(3)!=null){
						 hash = personal.getBlob(3);
						 Bitmap newBit=null;
						 Bitmap bit= BitmapFactory.decodeByteArray(hash, 0, hash.length);
						 if (bit != null) {
			        			newBit = Bitmap.createScaledBitmap(bit, 90, 90, true);
			        			bit.recycle();
			        		}
			        		if (newBit != null) {
			        			img.setImageBitmap(newBit);
			        		}
					 }
				 }
				personal.close();
				personal.deactivate();
				
				Cursor emergency= HopeApplication.db.query("emergency_info", null, null, null, null, null, null);
				 while (emergency.moveToNext()) {
					 if(emergency.getString(0)!=null){
						 cname.setText(emergency.getString(0));
					 }
					 if(emergency.getString(1)!=null){
						 cphone.setText(emergency.getString(1));
					 }
					 
				 }
				 emergency.close();
				 emergency.deactivate();
				
				 if(cname.getText().toString()==null || cname.getText().toString().equals("") || 
						 cphone.getText().toString()==null || cphone.getText().toString().equals("") ){
					 //If table does not contain data, show from preferences
					 SharedPreferences eConPreference = getSharedPreferences(
		                       "eConPreference", Context.MODE_PRIVATE);
					 String eName = eConPreference.getString("eConName", "");
				      String eNumber = eConPreference.getString("eConNumber", "");
				      cname.setText(eName);
				      cphone.setText(eNumber);
				 }
				
				 Cursor medical= HopeApplication.db.query("medical_info", null, null, null, null, null, null);
				 while (medical.moveToNext()) {
					 if(medical.getString(0)!=null){
						 bloodtype.setText(medical.getString(0));
					 } 
				 } 
				 medical.close();
				 medical.deactivate();
				
			} catch (Exception e) {
				 Toast.makeText(this, "No Data", Toast.LENGTH_SHORT).show();
			}
	}
	
	@Override
	public void onInit(int status) {
		
		
		
		// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
        if (status == TextToSpeech.SUCCESS) {
            // Set preferred language to US english.
            // Note that a language may not be available, and the result will indicate this.
            int result = mTts.setLanguage(Locale.US);
            
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED) {
               
            } else {
               
            }
        } else {
          
        }
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	    case R.id.configuration_settings:
	    	showConfiguration_settings();
	    	
	        return true;
	    case R.id.help:
	        showHelp();
	        return true;
	    case R.id.search:
	        showSearch();
	        return true;
	    case R.id.bookmark:
	    	addToFavorites();
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
		private void addToFavorites() {
		// TODO Auto-generated method stub
		
	}

		private void showSearch() {
			
				SentenceGeneration.clearAllApplicationVariables();
				Intent intent = new Intent().setClass(this,Search.class);
				
				startActivity(intent);
				
			
		}
		
	

		private void showHelp() {
		// TODO Auto-generated method stub
		
	}
	
		protected void onDestroy() {
	        super.onDestroy();
	        ImageView v = null;
	        v = (ImageView)img; 
	       ((BitmapDrawable) v.getDrawable()).setCallback(null);
	  }
		
		
}
