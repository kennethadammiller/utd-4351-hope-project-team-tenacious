package com.utdallas.hope.controller;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.utdallas.hope.R;
import com.utdallas.hope.controller.ShowMaps.ReverseGeocode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
 
public class FallDetectionOverview extends MapActivity {
	TextView result;
	TextView battery;
	TextView locationM;
	ImageView fallimage;
	private SensorManager sensorManager;
	private Sensor sensor;
	private float x, y, z;
	public float origX,origY,origZ;
	boolean call_yes=false;
	Context con;
	public MediaPlayer mp;
	public boolean callNow = true;
	float batteryPct;
    String locationStr="";
    LocationManager locationManager1;
    LocationListener locationListener1;
    AlertDialog.Builder andDialog;
    boolean isFall=false;
    MapView mapView;
    MapController mc;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor);
		con=this;
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
       
		result = (TextView) findViewById(R.id.result);
		battery= (TextView) findViewById(R.id.battery);
		locationM= (TextView) findViewById(R.id.locationM);
		fallimage= (ImageView) findViewById(R.id.fallimage);
		result.setText(" Fall Detected: No ");
		fallimage.setVisibility(fallimage.GONE);
		
		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = this.registerReceiver(null, ifilter);
		int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		batteryPct = level / (float)scale;
		batteryPct=0.45f;
		battery.setText("Battery Level: " + batteryPct);
		 ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo info = cm.getActiveNetworkInfo();

		battery.append("    Network type: " + info.getTypeName());
		
		  mapView = (MapView)findViewById(R.id.mapView);
          mapView.setClickable(true);
          mapView.setBuiltInZoomControls(true);
          mapView.displayZoomControls(true);
		// Acquire a reference to the system Location Manager
		 locationManager1 = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		// Define a listener that responds to location updates
	
	}
 
	
	void clearScreen(DialogInterface dialog){
		dialog.cancel();
	}
	
	  void callAlertDialog(){
		   andDialog =  new AlertDialog.Builder(con);
		  andDialog
	  	.setTitle("Fall Detected")
		.setMessage("Click OK to confirm the fall and send an alert")
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int whichButton) {	
//		    	mp = MediaPlayer.create(con, R.raw.panic);
//				mp.start();		
		    	result.setText(" Fall Detected and Confirmed");
		    	  detectLocation();
		       dialog.dismiss();
		    	//FallDetection.this.finish(); 
		       if(isFall){
					isFall=false;
				}
		      
		    	
		    }})
		 .setNegativeButton(android.R.string.no, null).show();
		 
		  }
 
	  
	  void detectLocation(){
	
		  locationListener1 = new LocationListener() {
			    @Override
				public void onLocationChanged(Location location) {
			      // Called when a new location is found by the network location provider.
			    	makeUseOfNewLocation(location);
			    }

			    @Override
				public void onStatusChanged(String provider, int status, Bundle extras) {}

			    @Override
				public void onProviderEnabled(String provider) {}

			    @Override
				public void onProviderDisabled(String provider) {}
			  };
			   if(batteryPct>=0.4){
			// Register the listener with the Location Manager to receive location updates
				locationManager1.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener1);
			   }
			   else{
				   locationManager1.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener1);
			   }
	  }
	  
	  void makeUseOfNewLocation(Location loc){
			
			try {
				Geocoder gc = new Geocoder(getBaseContext(), Locale.getDefault());
			    List<Address> myaddr = null;

				locationStr="";
				try {
					myaddr = gc.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
				} catch (IOException e) {
					
					myaddr =  ReverseGeocode.getFromLocation(loc.getLatitude(), 
								loc.getLongitude(), 3);
					} 
							
				if(myaddr.get(0).getAddressLine(0)!=null){
					locationStr = locationStr + myaddr.get(0).getAddressLine(0) + " ";
				}
				if(myaddr.get(0).getAdminArea()!=null){
					locationStr = locationStr + myaddr.get(0).getAdminArea() + " ";
				}
				if(myaddr.get(0).getLocality()!=null){
					locationStr = locationStr + myaddr.get(0).getLocality()+ " ";
				}
				if(myaddr.get(0).getPostalCode()!=null){
					locationStr = locationStr + myaddr.get(0).getPostalCode() + " ";
				}
				if(myaddr.get(0).getCountryName()!=null){
					locationStr = locationStr + myaddr.get(0).getCountryName();
				}
			    if(!locationStr.equals("")){
			    	locationM.setText("");
			    	locationM.setText(" Getting GPS Location : " + locationStr );
				locationManager1.removeUpdates(locationListener1);
				
				mapView.setStreetView(true);
				
				
					
					int p1= (int) (loc.getLatitude()*1e6);
					int p2 = (int) (loc.getLongitude()*1e6);
					GeoPoint point = new GeoPoint(p1,p2);
					
					mc = mapView.getController();
					mc.setZoom(17);
					mc.animateTo(point);
					
					mapView.invalidate();
				
				}else{
				// Register the listener with the Location Manager to receive location updates
					 if(batteryPct>=0.8){
						 locationManager1.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					 }else{
					locationManager1.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					 }
				
			    }
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
				
			}
			
	 }

	  public static class ReverseGeocode {

		    public static List<Address> getFromLocation(double lat, double lon, int maxResults) {
		    
		        String urlStr = "http://maps.google.com/maps/geo?q=" + lat + "," + lon + "&output=json&sensor=false";
		                String response = "";
		                List<Address> results = new ArrayList<Address>();
		                HttpClient client = new DefaultHttpClient();
		                
		                Log.d("ReverseGeocode", urlStr);
		                try {
		                        HttpResponse hr = client.execute(new HttpGet(urlStr));
		                        HttpEntity entity = hr.getEntity();

		                        BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));

		                        String buff = null;
		                        while ((buff = br.readLine()) != null)
		                                response += buff;
		                } catch (IOException e) {
		                        e.printStackTrace();
		                }

		                JSONArray responseArray = null;
		                try {
		                        JSONObject jsonObject = new JSONObject(response);
		                        responseArray = jsonObject.getJSONArray("Placemark");
		                } catch (JSONException e) {
		                        return results;
		                }

		                Log.d("ReverseGeocode", "" + responseArray.length() + " result(s)");
		                
		                for(int i = 0; i < responseArray.length() && i < maxResults-1; i++) {
		                        Address addy = new Address(Locale.getDefault());

		                        try {
		                                JSONObject jsl = responseArray.getJSONObject(i);

		                                String addressLine = jsl.getString("address");
		 
		                                if(addressLine.contains(","))
		                                        addressLine = addressLine.split(",")[0];

		                                addy.setAddressLine(0, addressLine);

		                                jsl = jsl.getJSONObject("AddressDetails").getJSONObject("Country");
		                                if(jsl!=null){
			                                addy.setCountryName(jsl.getString("CountryName"));
			                                addy.setCountryCode(jsl.getString("CountryNameCode"));
		                                }

		                                jsl = jsl.getJSONObject("AdministrativeArea");
		                                if(jsl!=null){
		                                	addy.setAdminArea(jsl.getString("AdministrativeAreaName"));
		                                }

		                                jsl = jsl.getJSONObject("Locality");
		                                if(jsl!=null){
		                                addy.setLocality(jsl.getString("LocalityName"));
		                                }

		                                if(jsl.getJSONObject("PostalCode").getString("PostalCodeNumber")!=null){
		                                	addy.setPostalCode(
		                                		jsl.getJSONObject("PostalCode").getString("PostalCodeNumber"));
		                                }
		                               

		                        } catch (JSONException e) {
		                                e.printStackTrace();
		                                continue;
		                        }

		                        results.add(addy);
		                }

		                return results;
		        }
		}
	@Override
	protected void onResume() {
		super.onResume();
		sensorManager.registerListener(accelerationListener, sensor,
				SensorManager.SENSOR_DELAY_GAME);
	}
 
	@Override
	protected void onStop() {
		sensorManager.unregisterListener(accelerationListener);
		super.onStop();
	}
 
	private SensorEventListener accelerationListener = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(Sensor sensor, int acc) {
		}
 
		@Override
		public void onSensorChanged(SensorEvent event) {
			float diffX,diffY,diffZ;
			x = event.values[0];
			y = event.values[1];
			z = event.values[2];
			if(origX==0){
				origX = x;
				diffX = 0;
			}else{
				diffX = Math.abs(origX-x);				
				origX= x;
			}
			if(origY == 0){
				origY = y;
				diffY = 0;
			}else{
				diffY = Math.abs(origY-y);				
				origY = y;
			}
			if(origZ == 0){
				origZ = z;
				diffZ = 0;
			}else{
				diffZ = Math.abs(origZ-z);				
				origZ = z;
			}
			if((diffZ>2&&diffY>2)||(diffZ>2&&diffX>2)||(diffY>2&&diffX>2)){
				Log.d("Log","Fall Detected!!!!");
				
				try {
					//play the panic sound file	
					if(!isFall){
					  callAlertDialog();
					  isFall=true;
					  fallimage.setVisibility(fallimage.VISIBLE);
					}
				     // onStop();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
										
			}
			
			
		}
 
	};
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}