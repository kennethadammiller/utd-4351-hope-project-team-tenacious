package com.utdallas.hope.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.gridModel.UtilitiesThings;
import com.utdallas.hope.tabParent.Emergency;
import com.utdallas.hope.tabParent.FallDetection;
import com.utdallas.hope.tabParent.SentenceGeneration;


public class UtilitiesThingsActivity extends Activity {
Context con=this;
GridView thingsGrid;
private static final int PREFERENCE = 12;


	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       
	        setContentView(R.layout.thingsactivity);
	       
			 thingsGrid = (GridView) findViewById(R.id.ThingsGrid);

			thingsGrid.setAdapter(new UtilitiesThings(thingsGrid.getContext(),
					getLayoutInflater()));
			
			
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					backCode();
				}
			});

			thingsGrid.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					if(HopeApplication.currentDisplayedUtilitiesThings.get(position).equalsIgnoreCase("DateTime"))
					{
						
						Intent startAgain = new Intent().setClass(getBaseContext(),DateTime.class);
		                
		                startActivity(startAgain);
						
					}
					if(HopeApplication.currentDisplayedUtilitiesThings.get(position).equalsIgnoreCase("Weather"))
					{
						//Temporary shows analysis screen
						Intent startAgain = new Intent().setClass(getBaseContext(),Analysis.class);
		                
		                startActivity(startAgain);
						
					}
					
					if(HopeApplication.currentDisplayedUtilitiesThings.get(position).equalsIgnoreCase("Fall Detection"))
					{
						
						Intent startAgain = new Intent().setClass(getBaseContext(),FallDetection.class);
		                
		                startActivity(startAgain);
						
					}
										
	if(HopeApplication.uthings.size()>0)
	{
						TextView tv3= (TextView)getParent().findViewById(R.id.title);
						tv3.setText("Utilities > Things >"+HopeApplication.uthings.get(0));
	}
	else
	{
	TextView tv3= (TextView)getParent().findViewById(R.id.title);
	tv3.setText("Utilities > Things");
	}
					
				}

			});

	  }
	  
	  
	  @Override
	protected void onDestroy() {
	        super.onDestroy();
	        final GridView grid = thingsGrid;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }
	  }

	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event) {
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	  	   backCode();
	      }
	      return super.onKeyDown(0, null);
}
	
	      void backCode(){
	    	  if (!HopeApplication.uthings.isEmpty()) {
	  	  		
		  			if (HopeApplication.uthings.size() == 1) {

		  			
		  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
		  				thingsGrid.setAdapter(new UtilitiesThings(
		  						thingsGrid.getContext(),
		  						getLayoutInflater()));
		  			
		  			

		  			} else {
		  				
		  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
		  				thingsGrid.setAdapter(new UtilitiesThings(
		  						thingsGrid.getContext(),
		  						getLayoutInflater(), HopeApplication.uthings
		  								.get(HopeApplication.uthings.size() - 1)));

		  				HopeApplication.uthings.remove(HopeApplication.uthings.size() - 1);
		  				

		  			}
	  		
		  	} else {

		  	}
		  	
		  	if(HopeApplication.uthings.size()>0)
		  	{
		  						TextView tv3= (TextView)getParent().findViewById(R.id.title);
		  						tv3.setText("Utilities > Things >"+HopeApplication.uthings.get(0));
		  	}
		  	else
		  	{
		  		TextView tv3= (TextView)getParent().findViewById(R.id.title);
		  		tv3.setText("Utilities > Things");
		  		}
		      
	      }
	 

	
		
	  @Override
     	public void onActivityResult(int requestCode, int resultCode, Intent data) {
     		// TODO Auto-generated method stub
     		super.onActivityResult(requestCode, resultCode, data);
     	
     	if(requestCode == PREFERENCE){
     		SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
	}
     	}
}
