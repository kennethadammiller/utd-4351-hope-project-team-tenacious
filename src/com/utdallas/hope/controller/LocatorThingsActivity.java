package com.utdallas.hope.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.gridModel.LocatorThings;
import com.utdallas.hope.tabParent.SentenceGeneration;


public class LocatorThingsActivity extends Activity {
Context con=this;
GridView thingsGrid;
private static final int PREFERENCE = 12;


	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       
	        setContentView(R.layout.thingsactivity);
	       
			 thingsGrid = (GridView) findViewById(R.id.ThingsGrid);

			thingsGrid.setAdapter(new LocatorThings(thingsGrid.getContext(),
					getLayoutInflater()));
			
			
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					backCode();
				}
			});

			thingsGrid.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					if(HopeApplication.currentDisplayedLocatorThings.get(position).equalsIgnoreCase("Current Location"))
					{
						
						Intent intent = new Intent(con,ShowMaps.class);
						startActivity(intent);
						
					}
					if(HopeApplication.currentDisplayedLocatorThings.get(position).equalsIgnoreCase("Get Direction"))
					{
						
					//	Intent intent = new Intent(con,ShowDirections.class);
					//	startActivity(intent);
						
					}
										
					if(HopeApplication.lthings.size()>0)
					{
										TextView tv3= (TextView)getParent().findViewById(R.id.title);
										tv3.setText("Locator > Things >"+HopeApplication.lthings.get(0));
					}
					else
					{
					TextView tv3= (TextView)getParent().findViewById(R.id.title);
					tv3.setText("Locator > Things");
					}
					
				}

			});

	  }
	  
	  
	  @Override
	protected void onDestroy() {
	        super.onDestroy();
	        final GridView grid = thingsGrid;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }
	  }

	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event) {
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	  	   backCode();
	      }
	      return super.onKeyDown(0, null);
}
	
	      void backCode(){
	    	  if (!HopeApplication.lthings.isEmpty()) {
	  	  		
		  			if (HopeApplication.lthings.size() == 1) {

		  			
		  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
		  				thingsGrid.setAdapter(new LocatorThings(
		  						thingsGrid.getContext(),
		  						getLayoutInflater()));
		  			
		  			

		  			} else {
		  				
		  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
		  				thingsGrid.setAdapter(new LocatorThings(
		  						thingsGrid.getContext(),
		  						getLayoutInflater(), HopeApplication.lthings
		  								.get(HopeApplication.lthings.size() - 1)));

		  				HopeApplication.lthings.remove(HopeApplication.lthings.size() - 1);
		  				

		  			}
	  		
		  	} else {

		  	}
		  	
		  	if(HopeApplication.lthings.size()>0)
		  	{
		  						TextView tv3= (TextView)getParent().findViewById(R.id.title);
		  						tv3.setText("Locator > Things >"+HopeApplication.lthings.get(0));
		  	}
		  	else
		  	{
		  		TextView tv3= (TextView)getParent().findViewById(R.id.title);
		  		tv3.setText("Locator > Things");
		  		}
		      
	      }
	 

	
		
	  @Override
     	public void onActivityResult(int requestCode, int resultCode, Intent data) {
     		// TODO Auto-generated method stub
     		super.onActivityResult(requestCode, resultCode, data);
     	
     	if(requestCode == PREFERENCE){
     		 SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
	}
     	}
}
