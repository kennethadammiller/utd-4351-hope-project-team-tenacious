package com.utdallas.hope.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.dialogDisplay.NameLocation;
import com.utdallas.hope.dialogDisplay.SingularPlural;
//import com.utdallas.hope.mapHelper.LocationItemOverlay;
import com.utdallas.hope.tabParent.SentenceGeneration;




public class ShowMaps extends MapActivity implements TextToSpeech.OnInitListener{
	Context con=this;
	 String  locationStr ="";
	 public TextToSpeech mTts;
	TextView currentLoc;
	static boolean locObtained = false;
    MapView mapView;
    MapController mc;
	LocationManager locationManager1;
	GeoPoint p ;
	LocationListener locationListener1;
	List<Overlay> mapOverlays;
	//LocationItemOverlay itemizedoverlay;
	private static final int PREFERENCE = 12;
	Button saveLoc ;
	double currentLat=0.0, currentLong=0.0;
		  @Override
		  public void onCreate(Bundle savedInstanceState) {
		       super.onCreate(savedInstanceState);
		        setContentView(R.layout.locator);
		        
				 mTts = new TextToSpeech(this,
				            this  // TextToSpeech.OnInitListener
				            );
		        currentLoc = (TextView)findViewById(R.id.currentLoc);
                mapView = (MapView)findViewById(R.id.mapView);
                mapView.setClickable(true);
                mapView.setBuiltInZoomControls(true);
                mapView.displayZoomControls(true);
               mapOverlays = mapView.getOverlays();
                Drawable drawable = this.getResources().getDrawable(R.drawable.icon);
              // itemizedoverlay = new LocationItemOverlay(drawable);
					// Acquire a reference to the system Location Manager
				 locationManager1 = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
				// Define a listener that responds to location updates
				 locationListener1 = new LocationListener() {
				    @Override
					public void onLocationChanged(Location location) {
				      // Called when a new location is found by the network location provider.
				    	makeUseOfNewLocation(location);
				    }

				    @Override
					public void onStatusChanged(String provider, int status, Bundle extras) {}

				    @Override
					public void onProviderEnabled(String provider) {}

				    @Override
					public void onProviderDisabled(String provider) {}
				  };

				// Register the listener with the Location Manager to receive location updates
					locationManager1.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener1);
					

				ImageButton bck = (ImageButton) findViewById(R.id.back);
				bck.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						locationManager1.removeUpdates(locationListener1);
						finish();
					}
				});
				
				Button speakLoc = (Button) findViewById(R.id.speakLoc);
				speakLoc.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						mTts.speak(currentLoc.getText().toString(),
					            TextToSpeech.QUEUE_FLUSH,  
					            null);
					}
				});
				
				 saveLoc = (Button) findViewById(R.id.saveLoc);
				saveLoc.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						//code for saving the current location
						saveLocation();
					}
				});
				
				if(!locationStr.equals("")){
					currentLoc.setText(locationStr);
					saveLoc.setEnabled(true);
					locationManager1.removeUpdates(locationListener1);
				}else{
				// Register the listener with the Location Manager to receive location updates
					locationManager1.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				}
	
		  }

		  
		  void saveLocation(){
			  boolean alreadyPresent=false;
			  Cursor myLocations= HopeApplication.db.query("my_locations", null, null, null, null, null, null);
				 while (myLocations.moveToNext()) {
					 if(myLocations.getDouble(1)==currentLat && myLocations.getDouble(2)==currentLong ){
						 alreadyPresent=true;
						 break;
					 }	 
				 }
				 myLocations.close();
				 myLocations.deactivate();
				 if(!alreadyPresent){
					 //Start new intent to display a dialog that allows name entry for this location
					 // or display dialog saying - Location already saved
					 Intent intent = new Intent().setClass(this,NameLocation.class);
					 intent.putExtra("currentAddress", currentLoc.getText().toString());
					 startActivityForResult(intent,31);
					
				 }else{
					 Intent intent = new Intent().setClass(this,NameLocation.class);
					 startActivity(intent);
				 }
		  }

		  @Override
		  public boolean onKeyDown(int keyCode, KeyEvent event) {
		      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
		  	   backCode();
		      }
		      return super.onKeyDown(0, null);
	}
		
		      void backCode(){
		    	  finish();    
		      }
		      
		  void makeUseOfNewLocation(Location loc){
				
				try {
					Geocoder gc = new Geocoder(getBaseContext(), Locale.getDefault());
				    List<Address> myaddr = null;

					locationStr="";
					try {
						myaddr = gc.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
					} catch (IOException e) {
						
						myaddr =  ReverseGeocode.getFromLocation(loc.getLatitude(), 
									loc.getLongitude(), 3);
						} 
					
					locationStr="";
				
					
					if(myaddr.get(0).getAddressLine(0)!=null){
						locationStr = locationStr + myaddr.get(0).getAddressLine(0) + " ";
					}
					if(myaddr.get(0).getAdminArea()!=null){
						locationStr = locationStr + myaddr.get(0).getAdminArea() + " ";
					}
					if(myaddr.get(0).getLocality()!=null){
						locationStr = locationStr + myaddr.get(0).getLocality()+ " ";
					}
					if(myaddr.get(0).getPostalCode()!=null){
						locationStr = locationStr + myaddr.get(0).getPostalCode() + " ";
					}
					if(myaddr.get(0).getCountryName()!=null){
						locationStr = locationStr + myaddr.get(0).getCountryName();
					}
					
					currentLoc.setText(locationStr);
					if(currentLoc.getText().toString().equals("")){
						saveLoc.setEnabled(false);
					}else{
						saveLoc.setEnabled(true);
					}
					mapView.setStreetView(true);
				
					if(loc!=null){
						if(!mapOverlays.isEmpty()){
							mapOverlays.removeAll(null);
							
						}
						currentLat= loc.getLatitude();
					    currentLong= loc.getLongitude();
						int p1= (int) (loc.getLatitude()*1e6);
						int p2 = (int) (loc.getLongitude()*1e6);
						GeoPoint point = new GeoPoint(p1,p2);
						OverlayItem overlayitem = new OverlayItem(point, "This is where you are", "Your location !");
						//itemizedoverlay.addOverlay(overlayitem);
						//mapOverlays.add(itemizedoverlay);
						mc = mapView.getController();
						mc.setZoom(17);
						mc.animateTo(point);
						saveLoc.setEnabled(true);
						mapView.invalidate();
					}
				
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
					
				}
				
		 }

		  public static class ReverseGeocode {

			    public static List<Address> getFromLocation(double lat, double lon, int maxResults) {
			    
			        String urlStr = "http://maps.google.com/maps/geo?q=" + lat + "," + lon + "&output=json&sensor=false";
			                String response = "";
			                List<Address> results = new ArrayList<Address>();
			                HttpClient client = new DefaultHttpClient();
			                
			                Log.d("ReverseGeocode", urlStr);
			                try {
			                        HttpResponse hr = client.execute(new HttpGet(urlStr));
			                        HttpEntity entity = hr.getEntity();

			                        BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));

			                        String buff = null;
			                        while ((buff = br.readLine()) != null)
			                                response += buff;
			                } catch (IOException e) {
			                        e.printStackTrace();
			                }

			                JSONArray responseArray = null;
			                try {
			                        JSONObject jsonObject = new JSONObject(response);
			                        responseArray = jsonObject.getJSONArray("Placemark");
			                } catch (JSONException e) {
			                        return results;
			                }

			                Log.d("ReverseGeocode", "" + responseArray.length() + " result(s)");
			                
			                for(int i = 0; i < responseArray.length() && i < maxResults-1; i++) {
			                        Address addy = new Address(Locale.getDefault());

			                        try {
			                                JSONObject jsl = responseArray.getJSONObject(i);

			                                String addressLine = jsl.getString("address");
			 
			                                if(addressLine.contains(","))
			                                        addressLine = addressLine.split(",")[0];

			                                addy.setAddressLine(0, addressLine);

			                                jsl = jsl.getJSONObject("AddressDetails").getJSONObject("Country");
			                                if(jsl!=null){
				                                addy.setCountryName(jsl.getString("CountryName"));
				                                addy.setCountryCode(jsl.getString("CountryNameCode"));
			                                }

			                                jsl = jsl.getJSONObject("AdministrativeArea");
			                                if(jsl!=null){
			                                	addy.setAdminArea(jsl.getString("AdministrativeAreaName"));
			                                }

			                                jsl = jsl.getJSONObject("Locality");
			                                if(jsl!=null){
			                                addy.setLocality(jsl.getString("LocalityName"));
			                                }

			                                if(jsl.getJSONObject("PostalCode").getString("PostalCodeNumber")!=null){
			                                	addy.setPostalCode(
			                                		jsl.getJSONObject("PostalCode").getString("PostalCodeNumber"));
			                                }
			                               

			                        } catch (JSONException e) {
			                                e.printStackTrace();
			                                continue;
			                        }

			                        results.add(addy);
			                }

			                return results;
			        }
			}
			
		  @Override
	     	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	     		// TODO Auto-generated method stub
	     		super.onActivityResult(requestCode, resultCode, data);
	     	
	     	if(requestCode == PREFERENCE){
	     		 SentenceGeneration.clearAllApplicationVariables();
					finish();
					Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
					intent.putExtra("refresh", "features");
					startActivity(intent);
		    }
	     	if(requestCode == 31 && resultCode==1){
	     		 	ContentValues cv= new ContentValues();
					cv.put("locName" , NameLocation.myLocationName);
					cv.put("locLatitude" ,currentLat);
					cv.put("locLongitude" ,currentLong);
					cv.put("locAddress" ,currentLoc.getText().toString());
					HopeApplication.db.insert("my_locations", null, cv);
		    }
	    }

		@Override
		protected boolean isRouteDisplayed() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onInit(int status) {
			
			
			
			// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
	        if (status == TextToSpeech.SUCCESS) {
	            // Set preferred language to US english.
	            // Note that a language may not be available, and the result will indicate this.
	            int result = mTts.setLanguage(Locale.US);
	            
	            if (result == TextToSpeech.LANG_MISSING_DATA ||
	                result == TextToSpeech.LANG_NOT_SUPPORTED) {
	               
	            } else {
	               
	            }
	        } else {
	          
	        }
			
		}
	
}

