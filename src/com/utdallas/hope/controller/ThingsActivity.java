package com.utdallas.hope.controller;



import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.dialogDisplay.SingularPlural;
import com.utdallas.hope.gridModel.Things;


public class ThingsActivity extends Activity implements TextToSpeech.OnInitListener{
	
     TextToSpeech mTts;
 static Context thingsactivitycontext;
static GridView thingsGrid;

	
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	            
	       super.onCreate(savedInstanceState);
	       thingsactivitycontext = getBaseContext();
	        setContentView(R.layout.thingsactivity);
	        
	        mTts = new TextToSpeech(this, this);
		    thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
			String loadTable=getIntent().getStringExtra("loadTable");
			String searchTable=getIntent().getStringExtra("searchTable");
			if(loadTable==null)
			{
				if(searchTable!=null && !searchTable.equals("")){
					HopeApplication.searchThings=true;
					thingsGrid.setAdapter(new Things(thingsGrid.getContext(),
							getLayoutInflater(),searchTable));
					
				}else{
					thingsGrid.setAdapter(new Things(thingsGrid.getContext(),
					getLayoutInflater()));
				}
			}
			else
			{
				if(loadTable.equals("table"))
				{
					
					thingsGrid.setAdapter(new Things(thingsGrid.getContext(),
							getLayoutInflater(),HopeApplication.things.get(HopeApplication.things.size()-1)));
					
				}
				
				if(loadTable.equals("forVerbs"))
				{
					try {
						Cursor c1 = HopeApplication.db.rawQuery("select * from verbs_things where verbs='"+HopeApplication.selectedActionTable+"'", null);
						thingsGrid.setAdapter(new Things(thingsGrid.getContext(),
								getLayoutInflater(),c1,true));
						//Toast.makeText(this, "sel size "+c1.getCount(), Toast.LENGTH_SHORT).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						Toast.makeText(this, "exception ", Toast.LENGTH_SHORT).show();
					}
					
				}
				if(loadTable.equals("forQuestionVerb"))
				{
					try {
						//Toast.makeText(this, "z  "+HopeApplication.selectedActionTable, Toast.LENGTH_SHORT).show();
						Cursor c1 = HopeApplication.db.rawQuery("select * from verbs_things where verbs='"+HopeApplication.selectedActionTable+"'", null);
						thingsGrid.setAdapter(new Things(thingsGrid.getContext(),
								getLayoutInflater(),c1,true));
						//Toast.makeText(this, "sel size "+c1.getCount(), Toast.LENGTH_SHORT).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						Toast.makeText(this, "exception ", Toast.LENGTH_SHORT).show();
					}
					
				}
				
			}
		//Code for back button
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					
					backCode();
				}
			});
			
			thingsGrid.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View v,
						int position, long id) {
					SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(getBaseContext());
					boolean wordPreference = prefs.getBoolean("wordPref", true);
					if(wordPreference){
						if(HopeApplication.things.size()!=0)
						{
						onItemShortClick(parent,v,position,id);
				   		Intent intent = new Intent().setClass(thingsactivitycontext,SingularPlural.class);
						startActivityForResult(intent,3);
	
						}
						else
						{
							onItemShortClick(parent,v,position,id);
						}
					}
					else{
						gridItemClicked(parent, v, position, id);
					}
					return false;
				}
			});
			
			thingsGrid.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					gridItemClicked(parent, v, position, id);
					}

			});
  
	  }
	  @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	    {
		  
	   
	    if(resultCode==1)
		{   
			TextView tv1=(TextView)getParent().findViewById(R.id.acc);
			if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
			{
				
				tv1.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
				
			}
			

			if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
			{
				
				tv1.setText( HopeApplication.question+" do "+HopeApplication.pronoun+" "+HopeApplication.verb+" "+
						HopeApplication.object + " ? ");
				
				
			}
		}
	 
	    }
	  
	  
	  void gridItemClicked(AdapterView<?> parent, View v,
				int position, long id) {
		  
		  int newPosition = position;
		  TextView title= (TextView)getParent().findViewById(R.id.title);
			title.setText("Sentence Generation > Things");
			
			HopeApplication.newSentenceToAdd="";
			thingsGrid = (GridView) findViewById(R.id.ThingsGrid);

			TextView tv = (TextView) v.findViewById(R.id.icon_text);
			speakText(tv.getText().toString());
			TextView tv1= (TextView)getParent().findViewById(R.id.acc);

				try {
					//Added code to ensure the correctness of the clicks,
					//despite of the change in position of the grid
					 for(int i=0;i<HopeApplication.currentDisplayedThings.size();i++){
						 if(tv.getText().toString().equalsIgnoreCase(HopeApplication.currentDisplayedThings.get(i))){
							 //new position of the item in the grid
							 newPosition=i;
							 break;
						 }
					 }

					 //Added code to get the parent table for a particular item to retrieve the plural values
					 if(HopeApplication.things.size()>=1){
						 HopeApplication.rootTable = HopeApplication.things.get(HopeApplication.things.size()-1);
				}
					 Cursor c = HopeApplication.db.query(HopeApplication.currentDisplayedThings.get(newPosition),
							null, null, null, null, null, null);
					 
					
					HopeApplication.things.add(HopeApplication.currentDisplayedThings.get(newPosition));
					thingsGrid.setAdapter(new Things(
							thingsGrid.getContext(), getLayoutInflater(),
							HopeApplication.currentDisplayedThings.get(newPosition)));
							HopeApplication.object=HopeApplication.things.get(HopeApplication.things.size()-1);
							
				} catch (Exception e) {
					
					HopeApplication.object=tv.getText().toString();
				}
				
					
				if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
				{
					
					tv1.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
					
				}
				

				if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
				{
					tv1.setText( HopeApplication.question+" do "+HopeApplication.pronoun+" "+HopeApplication.verb+" "+
							HopeApplication.object + " ? ");	
				}	
	  }
	  
	  public void onItemShortClick(AdapterView<?> parent, View v,
				int position, long id) {
		  gridItemClicked(parent, v, position, id);
			}

	  @Override
	  public  boolean onKeyDown(int keyCode, KeyEvent event) {
		  
		  if ((keyCode == KeyEvent.KEYCODE_MENU)) {
			  
			  this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, event);
			  
			  }
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	  backCode();
	      }
	      return true;
}

    void backCode(){
    	  
    	if (!HopeApplication.things.isEmpty()) {
	  		
  			if (HopeApplication.things.size() <= 1) {

  				thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
  				thingsGrid.setAdapter(new Things(
  						thingsGrid.getContext(),
  						getLayoutInflater()));
  			
  				HopeApplication.things.remove(HopeApplication.things.get(HopeApplication.things.size() - 1));

  				TextView tv1= (TextView)getParent().findViewById(R.id.acc);
				HopeApplication.object="";
				HopeApplication.verb="";
				 tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);

  			} else {

  				HopeApplication.things.remove(HopeApplication.things.get(HopeApplication.things.size() - 1));
  				 thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
  				thingsGrid.setAdapter(new Things(
  						thingsGrid.getContext(),
  						getLayoutInflater(), HopeApplication.things
  								.get(HopeApplication.things.size() - 1)));
  				TextView tv1= (TextView)getParent().findViewById(R.id.acc);
				HopeApplication.object=HopeApplication.things.get(HopeApplication.things.size()-1);
				 tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);
  			}

  	} else {
  		
  		Toast.makeText(this, "Cannot go back",
  				Toast.LENGTH_SHORT).show();

  	}
  	
  	if(HopeApplication.things.size()>=1)
	{
		TextView title= (TextView)getParent().findViewById(R.id.title);
		title.setText("Sentence Generation > Things > "+HopeApplication.things.get(0));
		
	}
	else
	{
		TextView title= (TextView)getParent().findViewById(R.id.title);
		title.setText("Sentence Generation > Things ");
	}
      }

	@Override
	public void onInit(int status) {
		
		
		// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
        if (status == TextToSpeech.SUCCESS) {
            // Set preferred language to US english.
            // Note that a language may not be available, and the result will indicate this.
            int result = mTts.setLanguage(Locale.US);
            
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED) {
               
            } else {
               
            }
        } else {
          
        }
	}
	  
	private void speakText(String string) {
		 mTts.speak(string,
		            TextToSpeech.QUEUE_FLUSH,  
		            null);
		
	}

	  @Override
	    public void onDestroy() {
	        // Don't forget to shutdown!
	        if (mTts != null) {
	            mTts.stop();
	            mTts.shutdown();
	        }

	        super.onDestroy();
	        final GridView grid = thingsGrid;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }
	    }
	  
	  
	
}
