package com.utdallas.hope.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Contacts.People;
import android.provider.Contacts.PeopleColumns;
import android.provider.Contacts.PhonesColumns;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.gridModel.EmergencyThings;
import com.utdallas.hope.tabParent.Emergency;


public class EmergencyThingsActivity extends Activity {
Context con=this;
 String  locationStr ="";
public static LocationManager locationManager;
public static LocationListener locationListener;
	private static final int PICK_CONTACT = 2;
static boolean locObtained = false;
GridView thingsGrid;
boolean locationPreference=false;
boolean call_yes=false;



	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       
	        setContentView(R.layout.thingsactivity);
	        SharedPreferences prefs = PreferenceManager
            .getDefaultSharedPreferences(getBaseContext());
	        locationPreference = prefs.getBoolean("locationDetectionPref", true);
			if(locationPreference){
				// Acquire a reference to the system Location Manager
			 locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
			// Define a listener that responds to location updates
			 locationListener = new LocationListener() {
			    @Override
				public void onLocationChanged(Location location) {
			      // Called when a new location is found by the network location provider.
			    	makeUseOfNewLocation(location);
			    }

			    @Override
				public void onStatusChanged(String provider, int status, Bundle extras) {}

			    @Override
				public void onProviderEnabled(String provider) {}

			    @Override
				public void onProviderDisabled(String provider) {}
			  };

			// Register the listener with the Location Manager to receive location updates
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			 thingsGrid = (GridView) findViewById(R.id.ThingsGrid);

			thingsGrid.setAdapter(new EmergencyThings(thingsGrid.getContext(),
					getLayoutInflater()));
			
			boolean callConfirmationPref = prefs.getBoolean("callConfirmationPref", true);
			call_yes=false;
		    if(callConfirmationPref){
		    	
		    	callAlertDialog();
		     	
		    }else{
		    	call_yes=true;
		    }
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					backCode();
				}
			});

			thingsGrid.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					int newPosition = position;
					TextView tv = (TextView) v.findViewById(R.id.icon_text);
					//Added code to ensure the correctness of the clicks,
					//despite of the change in position of the grid
					 for(int i=0;i<HopeApplication.currentDisplayedEmergencyThings.size();i++){
						 if(tv.getText().toString().equalsIgnoreCase(HopeApplication.currentDisplayedEmergencyThings.get(i))){
							 //new position of the item in the grid
							 newPosition=i;
							 break;
						 }
					 }		
					 
					if(HopeApplication.currentDisplayedEmergencyThings.get(newPosition).equalsIgnoreCase("Panic"))
					{
						
						try {
							//play the panic sound file
							 MediaPlayer mp = MediaPlayer.create(con, R.raw.panic);
							    mp.start();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else 
					{
				
						TextView tv1= (TextView)getParent().findViewById(R.id.acc);
						tv1.setText(EmergencyThings.mThumbMsg.get(newPosition));
						String locationMessage="";
						if(locationPreference){
							if(!locationStr.equals("")){
								
								locationManager.removeUpdates(locationListener);
							}else{
								
								makeUseOfNewLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
							}
							locationMessage =  " I am at location : " + locationStr;
						}

					    sendMessageCall(locationMessage,newPosition);
					    
				}
										
	if(HopeApplication.ethings.size()>0)
	{
						TextView tv3= (TextView)getParent().findViewById(R.id.title);
						tv3.setText("Emergency > Things >"+HopeApplication.ethings.get(0));
	}
	else
	{
	TextView tv3= (TextView)getParent().findViewById(R.id.title);
	tv3.setText("Emergency > Things");
	}
					
				}

			});

	  }
	  void sendMessageCall(String locationMessage, int position){
		  
		  if(call_yes){
			  String message="";
				//start a new activity to send an sms and later call
				SmsManager sms = SmsManager.getDefault();
				
				message = "HOPE test message : " + EmergencyThings.mThumbMsg.get(position) +  locationMessage;
				
				//Getting the preferences
				 SharedPreferences eConPreference = getSharedPreferences(
	                       "eConPreference", Context.MODE_PRIVATE);
			      String eNumber = eConPreference.getString("eConNumber", "");
			      
			      if(eNumber!=null && !eNumber.equals("")){
				       sms.sendTextMessage(eNumber, null, message, null, null);
				        locationStr="";
				        
				        //After some time (e.g. 10 sec), call the contact
				      
				        try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						
						//Code for calling the contact
						
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						
				        callIntent.setData(Uri.parse(eNumber));
				        startActivity(callIntent);
			      }
			      else{
			    	  Toast.makeText(getBaseContext(), "Emergency Contact Not Selected ! Please select now", Toast.LENGTH_SHORT).show();
			    	  Intent intent = new Intent(Intent.ACTION_PICK, People.CONTENT_URI);
                  	startActivityForResult(intent, PICK_CONTACT);
			      }
		    }
	  }
	  void callAlertDialog(){
	  new AlertDialog.Builder(this)
  	.setTitle("Emergency Message/Call Confirmation")
	.setMessage("Do you want to send the message and call emergency contact?")
	.setIcon(android.R.drawable.ic_dialog_alert)
	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

	    public void onClick(DialogInterface dialog, int whichButton) {
	    	call_yes=true;
	    }})
	 .setNegativeButton(android.R.string.no, null).show();
	 
	  }
	  
	  
	  @Override
	protected void onDestroy() {
	        super.onDestroy();
	        final GridView grid = thingsGrid;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }
	  }

	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event) {
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	  	   backCode();
	      }
	      return super.onKeyDown(0, null);
}
	
	      void backCode(){
	    	  if (!HopeApplication.ethings.isEmpty()) {
	  	  		
		  			if (HopeApplication.ethings.size() == 1) {

		  			
		  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
		  				thingsGrid.setAdapter(new EmergencyThings(
		  						thingsGrid.getContext(),
		  						getLayoutInflater()));
		  			
		  			

		  			} else {
		  				
		  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
		  				thingsGrid.setAdapter(new EmergencyThings(
		  						thingsGrid.getContext(),
		  						getLayoutInflater(), HopeApplication.ethings
		  								.get(HopeApplication.ethings.size() - 1)));

		  				HopeApplication.ethings.remove(HopeApplication.ethings.size() - 1);
		  				

		  			}
	  		
		  	} else {

		  	}
		  	
		  	if(HopeApplication.ethings.size()>0)
		  	{
		  						TextView tv3= (TextView)getParent().findViewById(R.id.title);
		  						tv3.setText("Emergency > Things >"+HopeApplication.ethings.get(0));
		  	}
		  	else
		  	{
		  		TextView tv3= (TextView)getParent().findViewById(R.id.title);
		  		tv3.setText("Emergency > Things");
		  		}
		      
	      }
	  void makeUseOfNewLocation(Location loc){
			
			try {
				locationStr="";
				List<Address> myaddr = ReverseGeocode.getFromLocation(loc.getLatitude(), loc.getLongitude(), 3);
				
				if(myaddr.get(0).getAddressLine(0)!=null){
					locationStr = locationStr + myaddr.get(0).getAddressLine(0) + " ";
				}
				if(myaddr.get(0).getAdminArea()!=null){
					locationStr = locationStr + myaddr.get(0).getAdminArea() + " ";
				}
				if(myaddr.get(0).getLocality()!=null){
					locationStr = locationStr + myaddr.get(0).getLocality()+ " ";
				}
				if(myaddr.get(0).getPostalCode()!=null){
					locationStr = locationStr + myaddr.get(0).getPostalCode() + " ";
				}
				if(myaddr.get(0).getCountryName()!=null){
					locationStr = locationStr + myaddr.get(0).getCountryName();
				}
				
		
				
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
				Toast.makeText( getApplicationContext(), "Cannot get the address at this time", Toast.LENGTH_SHORT).show();
			}
			
	 }

	  public static class ReverseGeocode {

		    public static List<Address> getFromLocation(double lat, double lon, int maxResults) {
		        String urlStr = "http://maps.google.com/maps/geo?q=" + lat + "," + lon + "&output=json&sensor=false";
		                String response = "";
		                List<Address> results = new ArrayList<Address>();
		                HttpClient client = new DefaultHttpClient();
		                
		                Log.d("ReverseGeocode", urlStr);
		                try {
		                        HttpResponse hr = client.execute(new HttpGet(urlStr));
		                        HttpEntity entity = hr.getEntity();

		                        BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));

		                        String buff = null;
		                        while ((buff = br.readLine()) != null)
		                                response += buff;
		                } catch (IOException e) {
		                        e.printStackTrace();
		                }

		                JSONArray responseArray = null;
		                try {
		                        JSONObject jsonObject = new JSONObject(response);
		                        responseArray = jsonObject.getJSONArray("Placemark");
		                } catch (JSONException e) {
		                        return results;
		                }

		                Log.d("ReverseGeocode", "" + responseArray.length() + " result(s)");
		                
		                for(int i = 0; i < responseArray.length() && i < maxResults-1; i++) {
		                        Address addy = new Address(Locale.getDefault());

		                        try {
		                                JSONObject jsl = responseArray.getJSONObject(i);

		                                String addressLine = jsl.getString("address");
		 
		                                if(addressLine.contains(","))
		                                        addressLine = addressLine.split(",")[0];

		                                addy.setAddressLine(0, addressLine);

		                                jsl = jsl.getJSONObject("AddressDetails").getJSONObject("Country");
		                                if(jsl!=null){
			                                addy.setCountryName(jsl.getString("CountryName"));
			                                addy.setCountryCode(jsl.getString("CountryNameCode"));
		                                }

		                                jsl = jsl.getJSONObject("AdministrativeArea");
		                                if(jsl!=null){
		                                	addy.setAdminArea(jsl.getString("AdministrativeAreaName"));
		                                }

		                                jsl = jsl.getJSONObject("Locality");
		                                if(jsl!=null){
		                                addy.setLocality(jsl.getString("LocalityName"));
		                                }

		                                if(jsl.getJSONObject("PostalCode").getString("PostalCodeNumber")!=null){
		                                	addy.setPostalCode(
		                                		jsl.getJSONObject("PostalCode").getString("PostalCodeNumber"));
		                                }
		                               

		                        } catch (JSONException e) {
		                                e.printStackTrace();
		                                continue;
		                        }

		                        results.add(addy);
		                }

		                return results;
		        }
		}
		
	  @Override
     	public void onActivityResult(int requestCode, int resultCode, Intent data) {
     		// TODO Auto-generated method stub
     		super.onActivityResult(requestCode, resultCode, data);
     	if (requestCode == PICK_CONTACT) {
     		 if (resultCode == Activity.RESULT_OK) {
     	        Uri contactData = data.getData();
     	        Cursor c =  managedQuery(contactData, null, null, null, null);
     	        if (c.moveToFirst()) {
     	          String name = c.getString(c.getColumnIndexOrThrow(PeopleColumns.NAME));
     	          String number = c.getString(c.getColumnIndexOrThrow(PhonesColumns.NUMBER));
     	          if(number !=null && number.length()>0 && number.contains("-")){
     	          number = number.replaceAll("-", "");
     	          }
     	          number = "tel:" + number;
     	          SharedPreferences eConPreference = getSharedPreferences(
                     "eConPreference", Context.MODE_PRIVATE);
        		SharedPreferences.Editor editor = eConPreference
                     .edit();
        		editor.putString("eConName", name);
        		editor.putString("eConNumber", number);    
        		editor.commit();
        		
        		Intent startAgain = new Intent().setClass(getBaseContext(),Emergency.class);
                
                startActivity(startAgain);
                finish();
     	        }
     	      }
     		
     	 }
     	
     	}
}
