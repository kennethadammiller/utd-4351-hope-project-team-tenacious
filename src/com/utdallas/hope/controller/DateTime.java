package com.utdallas.hope.controller;

import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DigitalClock;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.menuFunction.Search;
import com.utdallas.hope.tabParent.SentenceGeneration;

public class DateTime extends Activity implements TextToSpeech.OnInitListener{
	public Context dateContext=this;
	public TextToSpeech mTts;
	private static final int PREFERENCE = 12;
	TextView date;
	String dateText="";
	String dateFormat = "EEEE, MMMM dd, yyyy ";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.datetime);
		 
		 mTts = new TextToSpeech(this,
		            this  // TextToSpeech.OnInitListener
		            );
			 
		
 date=(TextView)findViewById(R.id.date);
 
 dateText = (String)DateFormat.format(dateFormat, new Date());
 
 date.append(dateText);
  
  Button speak =(Button)findViewById(R.id.speakDateTime);
  
  ImageButton back =(ImageButton)findViewById(R.id.back);
  
  ImageButton home =(ImageButton)findViewById(R.id.home);
 

speak.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {
        
		mTts.speak(date.getText().toString(),
	            TextToSpeech.QUEUE_ADD,  
	            null);
		
		String time = (String)DateFormat.format("h:mmaa", new Date());
		time.replace(":", " ");
		
		mTts.speak(" Time is " +
				time,
	            TextToSpeech.QUEUE_ADD,  
	            null);

	}});


back.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {

		finish();
		
	}});

home.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View view) {

		SentenceGeneration.clearAllApplicationVariables();
		finish();
		Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
		startActivity(intent);
		
	}});

	}
	
	
	@Override
	public void onInit(int status) {

		// status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
        if (status == TextToSpeech.SUCCESS) {
            // Set preferred language to US english.
            // Note that a language may not be available, and the result will indicate this.
            int result = mTts.setLanguage(Locale.US);
            
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED) {
               
            } else {
               
            }
        } else {
          
        }
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	    case R.id.configuration_settings:
	    	showConfiguration_settings();
	    	
	        return true;
	    case R.id.help:
	        showHelp();
	        return true;
	    case R.id.search:
	        showSearch();
	        return true;
	    case R.id.bookmark:
	    	
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
		

		private void showSearch() {
			
				SentenceGeneration.clearAllApplicationVariables();
				Intent intent = new Intent().setClass(this,Search.class);
				
				startActivity(intent);

		}
		
		private void showHelp() {
		// TODO Auto-generated method stub
		
	}
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data)
		 {
			
			super.onActivityResult(requestCode, resultCode, data);
			
			   if( requestCode == PREFERENCE){
				   SentenceGeneration.clearAllApplicationVariables();
					finish();
					Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
					intent.putExtra("refresh", "features");
					startActivity(intent);
			    }
			 }


		private void showConfiguration_settings() {
			  		// TODO Auto-generated method stub
			  			Intent intent = new Intent().setClass(this,Preferences.class);
			  			
			  			startActivityForResult(intent,PREFERENCE);
			  		
			  	}
}
