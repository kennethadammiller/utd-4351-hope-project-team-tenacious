package com.utdallas.hope.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.provider.Contacts.PeopleColumns;
import android.provider.Contacts.PhonesColumns;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.menuFunction.Preferences;
import com.utdallas.hope.menuFunction.Search;
import com.utdallas.hope.tabParent.SentenceGeneration;

public class AboutMeEdit extends Activity {
	public Context aboutmeeditContext=this;
	 EditText name;
	 EditText age;
	 EditText address;
	 EditText cname;
	 EditText cphone;
	 EditText bloodtype;
	 ImageView img;
	 
	 private static final int PREFERENCE = 12;
	 private static final int PICK_CONTACT = 2;
	 private static final int SELECT_PICTURE =5;
	 private String selectedImagePath;
     private String filemanagerstring;
     private boolean hasImage=false;
     byte[] hash;
	 private void showConfiguration_settings() {
	 	  		// TODO Auto-generated method stub
	 	  			Intent intent = new Intent().setClass(this,Preferences.class);
	 	  			
	 	  			startActivityForResult(intent,PREFERENCE);
	 	  		
	 	  	}
	 
	//public TextToSpeech mTts;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutmeedit);

		  name=(EditText)findViewById(R.id.name);
		  age=(EditText)findViewById(R.id.age);
		  address=(EditText)findViewById(R.id.address);

		 cname=(EditText)findViewById(R.id.contactName);
		  cphone=(EditText)findViewById(R.id.phone);
		  bloodtype=(EditText)findViewById(R.id.bloodType);
		  img=(ImageView)findViewById(R.id.addImage);
		  ImageButton back =(ImageButton)findViewById(R.id.back);
		  
		  ImageButton home =(ImageButton)findViewById(R.id.home);
		
		  //Getting all the data (Filling the fields with existing data 
		  try {
				Cursor personal= HopeApplication.db.query("personal_info", null, null, null, null, null, null);
				 while (personal.moveToNext()) {
					 if(personal.getString(0)!=null){
						 name.setText(personal.getString(0));
					 }
					 if(personal.getString(1)!=null){
						 age.setText(personal.getString(1));
					 }
					 if(personal.getString(2)!=null){
						 address.setText(personal.getString(2));
					 }
					//Add code for image also 
					 if(personal.getBlob(3)!=null){
						 hasImage=true;
						 hash = personal.getBlob(3);
						 Bitmap newBit=null;
						 Bitmap bit= BitmapFactory.decodeByteArray(hash, 0, hash.length);
						 if (bit != null) {
			        			newBit = Bitmap.createScaledBitmap(bit, 90, 90, true);
			        			bit.recycle();
			        		}
			        		if (newBit != null) {
			        			img.setImageBitmap(newBit);
			        		}
						
					 }else{
						 hasImage=false;
					 }
				 }
				personal.close();
				personal.deactivate();
				
				Cursor emergency= HopeApplication.db.query("emergency_info", null, null, null, null, null, null);
				 while (emergency.moveToNext()) {
					 if(emergency.getString(0)!=null){
						 cname.setText(emergency.getString(0));
					 }
					 if(emergency.getString(1)!=null){
						 cphone.setText(emergency.getString(1));
					 }
					 
				 }
				 emergency.close();
				 emergency.deactivate();
				
				 if(cname.getText().toString()==null || cname.getText().toString().equals("") || 
						 cphone.getText().toString()==null || cphone.getText().toString().equals("") ){
					 //If table does not contain data, show from preferences
					 SharedPreferences eConPreference = getSharedPreferences(
		                       "eConPreference", Context.MODE_PRIVATE);
					 String eName = eConPreference.getString("eConName", "");
				      String eNumber = eConPreference.getString("eConNumber", "");
				      cname.setText(eName);
				      cphone.setText(eNumber);
				 }
				
				 Cursor medical= HopeApplication.db.query("medical_info", null, null, null, null, null, null);
				 while (medical.moveToNext()) {
					 if(medical.getString(0)!=null){
						 bloodtype.setText(medical.getString(0));
					 } 
				 } 
				 medical.close();
				 medical.deactivate();
				
			} catch (Exception e) {
				 Toast.makeText(this, "No Data", Toast.LENGTH_SHORT).show();
			}
			
		  
		
		  img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);

				
			}
		});
		 
		  Button selectContact =(Button)findViewById(R.id.selectContact);
		  selectContact.setOnClickListener(new View.OnClickListener() {
			 	@Override
				public void onClick(View view) {
			 		Intent intent = new Intent(Intent.ACTION_PICK, People.CONTENT_URI);
                	startActivityForResult(intent, PICK_CONTACT);
                   
			 	}});
		  
		  Button cancel =(Button)findViewById(R.id.cancelEdit);
		  cancel.setOnClickListener(new View.OnClickListener() {
			 	@Override
				public void onClick(View view) {
			 		finish();
			 	}});
		  
		  
		 Button save =(Button)findViewById(R.id.save1);
		 save.setOnClickListener(new View.OnClickListener() {
		 	@Override
			public void onClick(View view) {
		 		//save the entered data
		 		//saving personal data
		 		savePersonalDetails();
		 		//saving emergency contact data
		 		saveEmergencyContactDetails();
		 		//saving medical data
		 		saveMedicalDetails();

		 		finish();
	
		 	}});
		 
		 back.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					finish();
					
				}});

			home.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					SentenceGeneration.clearAllApplicationVariables();
					finish();
					Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
					startActivity(intent);
					
				}});

	}
	
	void savePersonalDetails(){
		HopeApplication.db.delete("personal_info", null, null);
		ContentValues cv= new ContentValues();
		cv.put("name" ,name.getText().toString());
		cv.put("age" ,age.getText().toString());
		cv.put("address" ,address.getText().toString());
		if(hasImage){
			cv.put("myImage" ,hash);
		}
		HopeApplication.db.insert("personal_info", null, cv);
	}
	
	
	void saveEmergencyContactDetails(){
		HopeApplication.db.delete("emergency_info", null, null);
		ContentValues cv= new ContentValues();
		cv.put("Name" ,cname.getText().toString());
		cv.put("Phone_Number" ,cphone.getText().toString());
		
		HopeApplication.db.insert("emergency_info", null, cv);
		
		//Adding to preferences 
		
		SharedPreferences eConPreference = getSharedPreferences(
                "eConPreference", Context.MODE_PRIVATE);
   		SharedPreferences.Editor editor = eConPreference
                .edit();
   		String number="";
		if(cphone.getText().toString() !=null && cphone.getText().toString().length()>0 ){
			if(cphone.getText().toString().contains("-")){
				number = cphone.getText().toString().replaceAll("-", "");
		          }
		}
		if(cname.getText().toString() !=null && !cname.getText().toString().equals("") && !number.equals("") ){
			 	number = "tel:" + number;
		   		editor.putString("eConName", cname.getText().toString());
		   		editor.putString("eConNumber", number);    
		   		editor.commit();
		}
	}
	
	
	void saveMedicalDetails(){
		HopeApplication.db.delete("medical_info", null, null);
		ContentValues cv= new ContentValues();
		cv.put("Blood_Type" ,bloodtype.getText().toString());
		
		HopeApplication.db.insert("medical_info", null, cv);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
				if (requestCode == SELECT_PICTURE ) {
					     if(data!=null){
			            Uri selectedImageUri = data.getData();
			
			            //OI FILE Manager
			            filemanagerstring = selectedImageUri.getPath();
			
			            //MEDIA GALLERY
			            selectedImagePath = getPath(selectedImageUri);
					     }
			            if(selectedImagePath!=null && !selectedImagePath.equals("")
			            		&& filemanagerstring!=null && !filemanagerstring.equals("") ){
							Bitmap newBit=null;
							 Bitmap bit= BitmapFactory.decodeFile(selectedImagePath);
							 if (bit != null) {
				        			newBit = Bitmap.createScaledBitmap(bit, 90, 90, true);
				        			bit.recycle();
				        		}
				        		if (newBit != null) {
				        			img.setImageBitmap(newBit);
				        		}
			
							FileInputStream fis;
							try {
								fis = new FileInputStream(selectedImagePath);
								hash = new byte[fis.available()];
								fis.read(hash);
								fis.close(); 
								hasImage=true;
							} catch (FileNotFoundException e) {
								finish();
								e.printStackTrace();
							} catch (IOException e) {
								finish();
								e.printStackTrace();
							}
		            }else{
		            	finish();
		            }
	
		        }
			
		if (requestCode == PICK_CONTACT) {
      		 if (resultCode == Activity.RESULT_OK) {
      	        Uri contactData = data.getData();
      	        Cursor c =  managedQuery(contactData, null, null, null, null);
      	        if (c.moveToFirst()) {
      	          String name = c.getString(c.getColumnIndexOrThrow(PeopleColumns.NAME));
      	          String number = c.getString(c.getColumnIndexOrThrow(PhonesColumns.NUMBER));
      	          if(number !=null && number.length()>0 && number.contains("-")){
      	          number = number.replaceAll("-", "");
      	          }
      	          cname.setText(name);
      	          cphone.setText(number);
      	        }
      	      c.close();
  	          c.deactivate();
      	      }
      		
      	 }
		if(requestCode == PREFERENCE){
			 SentenceGeneration.clearAllApplicationVariables();
				finish();
				Intent intent = new Intent().setClass(getBaseContext(),SentenceGeneration.class);
				intent.putExtra("refresh", "features");
				startActivity(intent);
	}
		
		

}
	
	public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if(cursor!=null)
        {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        else return null;
    }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	    case R.id.configuration_settings:
	    	showConfiguration_settings();
	    	
	        return true;
	    case R.id.help:
	        showHelp();
	        return true;
	    case R.id.search:
	        showSearch();
	        return true;
	    case R.id.bookmark:
	    	addToFavorites();
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
		private void addToFavorites() {
		// TODO Auto-generated method stub
		
	}

		private void showSearch() {
				SentenceGeneration.clearAllApplicationVariables();
				Intent intent = new Intent().setClass(this,Search.class);
				startActivity(intent);
		}
		
	

		private void showHelp() {
		// TODO Auto-generated method stub
		
	}
		//destroy method for freeing up image resources
		 protected void onDestroy() {
		        super.onDestroy();
		        ImageView v = null;
		        v = (ImageView)img; 
		        if(v!=null && v.getDrawable()!=null){
		        	((BitmapDrawable) v.getDrawable()).setCallback(null);
		        }
		  }
		  
	
}

