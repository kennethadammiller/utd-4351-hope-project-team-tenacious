package com.utdallas.hope.controller;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.dialogDisplay.AddedSentences;
import com.utdallas.hope.dialogDisplay.Favourites;
import com.utdallas.hope.gridModel.KeyPhraseThings;


public class KeyPhraseThingsActivity extends Activity {
Context con=this;
GridView thingsGrid;

	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       
	        setContentView(R.layout.thingsactivity);
			 thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
			 String searchTable = getIntent().getStringExtra("searchTable");
			 if(searchTable!=null && !searchTable.equals("")){
				 thingsGrid.setAdapter(new KeyPhraseThings(thingsGrid.getContext(),
							getLayoutInflater(),searchTable));
				
			 	}
			 else{
			thingsGrid.setAdapter(new KeyPhraseThings(thingsGrid.getContext(),
					getLayoutInflater()));
			 }
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					backCode();
				}
			});
			thingsGrid.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					int newPosition = position;
					TextView tv = (TextView) v.findViewById(R.id.icon_text);
					//Added code to ensure the correctness of the clicks,
					//despite of the change in position of the grid
					 for(int i=0;i<HopeApplication.currentDisplayedKeyPhraseThings.size();i++){
						 if(tv.getText().toString().equalsIgnoreCase(HopeApplication.currentDisplayedKeyPhraseThings.get(i))){
							 //new position of the item in the grid
							 newPosition=i;
							 break;
						 }
					 }
					if(HopeApplication.currentDisplayedKeyPhraseThings.get(newPosition).equalsIgnoreCase("added_sentences"))
					{
							Intent intent = new Intent().setClass(con,AddedSentences.class);
							startActivityForResult(intent,1);
					}
					
					if(HopeApplication.currentDisplayedKeyPhraseThings.get(newPosition).equalsIgnoreCase("favorites"))
					{
						Intent intent = new Intent().setClass(con,Favourites.class);
						startActivityForResult(intent,1);
					}
					else
					{
					TextView tv1= (TextView)getParent().findViewById(R.id.acc);
					
					try {
					
						Cursor c = HopeApplication.db.query(HopeApplication.currentDisplayedKeyPhraseThings.get(newPosition),
								null, null, null, null, null, null);
						
						if (c.getCount() > 0) {
							
							GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
							HopeApplication.kpthings.add(HopeApplication.currentDisplayedKeyPhraseThings.get(newPosition));

							thingsGrid.setAdapter(new KeyPhraseThings(
									thingsGrid.getContext(), getLayoutInflater(),
									HopeApplication.currentDisplayedKeyPhraseThings.get(newPosition)));							
							tv1.setText(tv.getText().toString());

						} else {

							tv1.setText(tv.getText().toString());
						}
						
	
					} catch (Exception e) {

						tv1.setText(tv.getText().toString());
					}
					}
					
					
				
if(HopeApplication.kpthings.size()>0)
{
					TextView tv3= (TextView)getParent().findViewById(R.id.title);
					tv3.setText("KeyPhrases > Things >"+HopeApplication.kpthings.get(0));
}
else
{
	TextView tv3= (TextView)getParent().findViewById(R.id.title);
	tv3.setText("KeyPhrases > Things");
	}
					
				}
				
			
				
			});
			
			
		  
	  }
	  
	  @Override
	protected void onDestroy() {
	        super.onDestroy();
	        final GridView grid = thingsGrid;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }

}  
	  
	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event) {
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {

	          backCode();
	      }
	      return super.onKeyDown(0, null);
}
	  
   void backCode(){
		if (!HopeApplication.kpthings.isEmpty()) {
	  		
  			if (HopeApplication.kpthings.size() == 1) {

  			
  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
  				thingsGrid.setAdapter(new KeyPhraseThings(
  						thingsGrid.getContext(),
  						getLayoutInflater()));

  				

  			} else {

  				GridView thingsGrid = (GridView) findViewById(R.id.ThingsGrid);
  				thingsGrid.setAdapter(new KeyPhraseThings(
  						thingsGrid.getContext(),
  						getLayoutInflater(), HopeApplication.kpthings
  								.get(HopeApplication.kpthings.size() - 1)));

  				HopeApplication.kpthings.remove(HopeApplication.kpthings.size() - 1);
  				

  			}

  		
  	} else {

  	}
  	
  	if(HopeApplication.kpthings.size()>0)
  	{
  						TextView tv3= (TextView)getParent().findViewById(R.id.title);
  						tv3.setText("KeyPhrases > Things >"+HopeApplication.kpthings.get(0));
  	}
  	else
  	{
  		TextView tv3= (TextView)getParent().findViewById(R.id.title);
  		tv3.setText("KeyPhrases > Things");
  		}
   }

	 
}
