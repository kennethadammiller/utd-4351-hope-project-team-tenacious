package com.utdallas.hope.controller;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.utdallas.hope.R;
import com.utdallas.hope.config.HopeApplication;
import com.utdallas.hope.dialogDisplay.Tense;
import com.utdallas.hope.gridModel.Actions;
import com.utdallas.hope.gridModel.Things;
import com.utdallas.hope.tabParent.SentenceGeneration;



public class ActionsActivity extends Activity {
	Context con=this;
    boolean hasComeHere=false;
	
    String searchTable="";
	GridView actionsGrid;



	
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	       
	        setContentView(R.layout.actionsactivity);
	    	actionsGrid = (GridView) findViewById(R.id.ActionsGrid);
	    	
	    	
	    	String loadTable=getIntent().getStringExtra("loadTable");
	    	searchTable=getIntent().getStringExtra("searchTable");
			if(loadTable==null)
			{
				if(searchTable!=null && !searchTable.equals("")){
					actionsGrid.setAdapter((ListAdapter) new Actions(actionsGrid.getContext(),
					getLayoutInflater(),searchTable));
				}
				else{
					actionsGrid.setAdapter((ListAdapter) new Actions(actionsGrid.getContext(),
							getLayoutInflater()));
				}
			}
			else
			{
				if(loadTable.equals("table"))
				{
					
					actionsGrid.setAdapter((ListAdapter) new Actions(actionsGrid.getContext(),
							getLayoutInflater(),HopeApplication.actions.get(HopeApplication.actions.size()-1)));
					
				}
				
				if(loadTable.equals("forThings"))
				{
					try {
						Cursor c1 = HopeApplication.db
						.rawQuery(
								"select * from things_verbs where things='"
										+ HopeApplication.things.get(0)
										+ "'", null);
						//Toast.makeText(con, "here "+HopeApplication.things.get(0)+" "+c1.getCount(), Toast.LENGTH_SHORT).show();
						
				
						
						actionsGrid.setAdapter((ListAdapter) new Actions(
								actionsGrid.getContext(),
								getLayoutInflater(), c1,
								"verbs"));
						//Toast.makeText(this, "sel size "+c1.getCount(), Toast.LENGTH_SHORT).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						Toast.makeText(this, "exception ", Toast.LENGTH_SHORT).show();
					}
					
				}
				
				
			}
			
			//Code for back button
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					backCode();
				}
			});
			
			actionsGrid.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View v,
						int position, long id) {
					SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(getBaseContext());
					boolean wordPreference = prefs.getBoolean("wordPref", true);
					if(wordPreference){
						if(HopeApplication.actions.size()!=0)
						{
							if(HopeApplication.actions.get(HopeApplication.actions.size()-1).equalsIgnoreCase("verbs"))
							{
							onItemShortClick(parent,v,position,id);
				   		Intent intent = new Intent().setClass(con,Tense.class);
				   		
						startActivityForResult(intent,4);
							}
						
						}
						else
						{
							onItemShortClick(parent,v,position,id);
						}
					}else{
						onItemShortClick(parent,v,position,id);
					}
					return false;
				}
			});
			
			
			
			actionsGrid.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					
					TextView title= (TextView)getParent().findViewById(R.id.title);
					title.setText("Sentence Generation > Actions");
					
					HopeApplication.newSentenceToAdd = "";
					Cursor c = null;
					TextView tv = (TextView) v.findViewById(R.id.icon_text);
					GridView actionsGrid = (GridView) findViewById(R.id.ActionsGrid);
					if(HopeApplication.things.size()==0)//things=0 
					{
					if(HopeApplication.actions.size()==0)//things=0 and actions=0
					{
						if (tv.getText().toString().equalsIgnoreCase("verbs")) {//things=0 and actions=0
							c = HopeApplication.db.query(
									"verbs", null, null, null, null,
									null, null);
							HopeApplication.actions.add("verbs");							
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "verbs"));
						}
						else if (tv.getText().toString().equalsIgnoreCase("comments")) {//things=0 and actions=0
							hasComeHere=true;
							c = HopeApplication.db.query("comment_category", null,
									null, null, null, null, null);
							HopeApplication.actions.add("comments");		//Change1					
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "comment_category"));
						}
						else if (tv.getText().toString().equalsIgnoreCase("questions")) {//things=0 and actions=0
							c = HopeApplication.db.query("questions", null,
									null, null, null, null, null);
							HopeApplication.actions.add("questions");							
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "questions"));
							

						}
						else{
							//Code only when it is from a search result
							if(HopeApplication.pronoun.equals("")){
								HopeApplication.pronoun="I";
							}
							 if(searchTable!=null && !searchTable.equals("")){
								if(searchTable.equalsIgnoreCase("verbs"))//things=0 and actions=1
								{
									HopeApplication.verb=tv.getText().toString();
									HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
								}
								if(searchTable.equalsIgnoreCase("comments"))//things=0 and actions=2
									{
										
										HopeApplication.verb=tv.getText().toString();//.substring(0,strsize-9
										HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
									}
								if(searchTable.equalsIgnoreCase("questions"))//things=0 and actions=1
								{
									HopeApplication.question=tv.getText().toString();
									c = HopeApplication.db.query(
											"verbs", null, null, null, null,
											null, null);
									HopeApplication.actions.add("questions");
									HopeApplication.actions.add("verbs");							
									actionsGrid.setAdapter((ListAdapter) new Actions(
									actionsGrid.getContext(),
									getLayoutInflater(), "verbs"));	

								}
						}
						}
					}
					else//things=0 and actions>0
					{
						if(HopeApplication.actions.size()==1)//things=0 and actions=1
						{
							if(HopeApplication.actions.get(0).equalsIgnoreCase("verbs"))//things=0 and actions=1
							{
								HopeApplication.verb=tv.getText().toString();
								HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
							}
							if(HopeApplication.actions.get(0).equalsIgnoreCase("comments"))//things=0 and actions=1
							{
								Cursor c1 = HopeApplication.db
								.rawQuery(
										"select * from things_comments where things='"
												+ HopeApplication.currentDisplayedActions.get(position)
												+ "'", null);
								HopeApplication.actions
								.add(HopeApplication.currentDisplayedActions
										.get(position));
						
								
								actionsGrid.setAdapter((ListAdapter) new Actions(
										actionsGrid.getContext(),
										getLayoutInflater(), c1,
										"comments"));
								
							}
							if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))//things=0 and actions=1
							{
								c = HopeApplication.db.query(
										"verbs", null, null, null, null,
										null, null);
								HopeApplication.actions.add("verbs");							
								actionsGrid.setAdapter((ListAdapter) new Actions(
								actionsGrid.getContext(),
								getLayoutInflater(), "verbs"));	
								//Toast.makeText(con, "questi", Toast.LENGTH_SHORT).show();
								HopeApplication.question=tv.getText().toString();
							}
						}
						else//things 0 action>1
						{
						if(HopeApplication.actions.size()==2)//things=0 and actions=2
						{
							if(HopeApplication.actions.get(0).equalsIgnoreCase("comments"))//things=0 and actions=2
							{
								
								HopeApplication.verb=tv.getText().toString();//.substring(0,strsize-9
								HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
							}
							if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))//things=0 and actions=2
							{
								HopeApplication.verb=tv.getText().toString();
								HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
							}
						}
						}
						
					}
						
					}
					else//things>0 
					{ 
						if(HopeApplication.actions.size()==0)//things>0 actions=0
						{
							//For search
							if(HopeApplication.searchThings){
								if (tv.getText().toString().equalsIgnoreCase("verbs")) {//things=0 and actions=0
									c = HopeApplication.db.query(
											"verbs", null, null, null, null,
											null, null);
									HopeApplication.actions.add("verbs");							
									actionsGrid.setAdapter((ListAdapter) new Actions(
									actionsGrid.getContext(),
									getLayoutInflater(), "verbs"));
							
									

								}
								if (tv.getText().toString().equalsIgnoreCase("comments")) {//things=0 and actions=0
									hasComeHere=true;
									c = HopeApplication.db.query("comments", null,
											null, null, null, null, null);
									HopeApplication.actions.add("comments");		//Change1					
									actionsGrid.setAdapter((ListAdapter) new Actions(
									actionsGrid.getContext(),
									getLayoutInflater(), "comments"));

								}
								if (tv.getText().toString().equalsIgnoreCase("questions")) {//things=0 and actions=0
									c = HopeApplication.db.query("questions", null,
											null, null, null, null, null);
									HopeApplication.actions.add("questions");							
									actionsGrid.setAdapter((ListAdapter) new Actions(
									actionsGrid.getContext(),
									getLayoutInflater(), "questions"));
									

								}
							}else{
							if (tv.getText().toString().equalsIgnoreCase("verbs")) {//things>0 actions=0
								
								Cursor c1 = HopeApplication.db
								.rawQuery(
										"select * from things_verbs where things='"
												+ HopeApplication.things.get(0)
												+ "'", null);
								//Toast.makeText(con, "here "+HopeApplication.things.get(0)+" "+c1.getCount(), Toast.LENGTH_SHORT).show();
								HopeApplication.actions
								.add("verbs");
						
								
								actionsGrid.setAdapter((ListAdapter) new Actions(
										actionsGrid.getContext(),
										getLayoutInflater(), c1,
										"verbs"));
							}
							
								if (tv.getText().toString().equalsIgnoreCase("comments")) {//things>0 actions=0
									Cursor c1 = HopeApplication.db
									.rawQuery(
											"select * from things_comments where things='"
													+ HopeApplication.things.get(0)
													+ "'", null);
									HopeApplication.actions
									.add("comments");
							
									
									actionsGrid.setAdapter((ListAdapter) new Actions(
											actionsGrid.getContext(),
											getLayoutInflater(), c1,
											"comments"));
								}
								if (tv.getText().toString().equalsIgnoreCase("questions")) {//things>0 actions=0
									c = HopeApplication.db.query("questions", null,
											null, null, null, null, null);
									HopeApplication.actions.add("questions");							
									actionsGrid.setAdapter((ListAdapter) new Actions(
									actionsGrid.getContext(),
									getLayoutInflater(), "questions"));
								}
							}
					}
						else
						{
						if(HopeApplication.actions.size()==1)//things>0 actions=1
						{
							if (HopeApplication.actions.get(0).equalsIgnoreCase("verbs")) {
							HopeApplication.verb=tv.getText().toString();
							HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
							}
							
							if (HopeApplication.actions.get(0).equalsIgnoreCase("comments")) {
								
								HopeApplication.verb=tv.getText().toString();
								HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
								
							}
							if (HopeApplication.actions.get(0).equalsIgnoreCase("questions")) {
								if(!HopeApplication.searchThings){
									
								
								Cursor c1 = HopeApplication.db.rawQuery(
										"select * from things_verbs where things='"
												+ HopeApplication.things.get(0)
												+ "'", null);
								HopeApplication.actions.add("verbs");
								
								HopeApplication.question=tv.getText().toString();//added to have question displayed on top
								
								actionsGrid.setAdapter((ListAdapter) new Actions(
										actionsGrid.getContext(),
										getLayoutInflater(), c1,
										"verbs"));
								}
								else{
								//Toast.makeText(con, "here "+HopeApplication.things.get(0)+" "+c1.getCount(), Toast.LENGTH_SHORT).show();
								HopeApplication.actions.add("verbs");
						
								HopeApplication.question=tv.getText().toString();//added to have question displayed on top
								
								actionsGrid.setAdapter((ListAdapter) new Actions(
										actionsGrid.getContext(),
										getLayoutInflater(), 
										"verbs"));
								}
							}
						}
						else if(HopeApplication.actions.size()==2)//things>0 actions=2
						{
							
							if (HopeApplication.actions.get(1).equalsIgnoreCase("verbs")) {
							HopeApplication.verb = tv.getText().toString();
							
							HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
							}
						}
						
						}
					}
					TextView tv1=(TextView)getParent().findViewById(R.id.acc);
					if(HopeApplication.actions.size()>0 )
					{
						if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))
						{
							HopeApplication.sentenceType="question";
						}
						else
						{
							HopeApplication.sentenceType="normal";
						}
					}
					if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
					{
						
						tv1.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
						
					}
					

					if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
					{
						
						tv1.setText( HopeApplication.question + " do " + HopeApplication.pronoun + " " 
								+ HopeApplication.verb + " " + HopeApplication.object + " ? ");
						
						
					}
					
				}});

	}
	  
	  
	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event) {
		  if ((keyCode == KeyEvent.KEYCODE_MENU)) {
		  
		  this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, event);
		  
		  }
		  
	      if ((keyCode == KeyEvent.KEYCODE_BACK)) {

	    	  backCode();
	  	
	      
	      }
	      return true;
}


	  void backCode(){
		  if(SentenceGeneration.mTabHost.getCurrentTab()==1){
			  if (!HopeApplication.actions.isEmpty()) {
		  		
	  			if (HopeApplication.actions.size() <= 1) {

	  			
	  				GridView actionsGrid = (GridView) findViewById(R.id.ActionsGrid);
	  				actionsGrid.setAdapter((ListAdapter) new Actions(
	  						actionsGrid.getContext(),
	  						getLayoutInflater()));
	  			
	  				HopeApplication.actions.remove(HopeApplication.actions.get(HopeApplication.actions.size() - 1));
	  				if(HopeApplication.actions.size()==0){
						HopeApplication.verb="";
					}
	  				
	  				TextView tv1= (TextView)getParent().findViewById(R.id.acc);
	  				 tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);

	  			} else {
	  				
	  				HopeApplication.actions.remove(HopeApplication.actions.get(HopeApplication.actions.size() - 1));
	  				GridView actionsGrid = (GridView) findViewById(R.id.ActionsGrid);
	  				
	  				
	  				actionsGrid.setAdapter((ListAdapter) new Actions(
	  						actionsGrid.getContext(),
	  						getLayoutInflater(), (String) HopeApplication.actions
	  								.get(HopeApplication.actions.size() - 1)));

	  				TextView tv1= (TextView)getParent().findViewById(R.id.acc);
					
					HopeApplication.verb=HopeApplication.actions.get(HopeApplication.actions.size()-1);
					if(HopeApplication.verb.equals("verbs") || HopeApplication.verb.equals("comments") 
							|| HopeApplication.verb.equals("questions")){
						HopeApplication.verb="";
					}
						//Changed to accommodate questions also
					// tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);
					 if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
						{	
							tv1.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
						}
						if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
						{
							tv1.setText( HopeApplication.question + " do " + HopeApplication.pronoun + " " 
									+ HopeApplication.verb + " " + HopeApplication.object + " ? ");
						}
	  			}

	  	} else {
	  		Toast.makeText(this, "Cannot go back",
	  				Toast.LENGTH_SHORT).show();

	  	}
	  	
	      
	  	if(HopeApplication.actions.size()>=1)
  	{
  		TextView title= (TextView)getParent().findViewById(R.id.title);
			title.setText("Sentence Generation > Actions > "+HopeApplication.actions.get(0));
			
  	}
  	else
  	{
  		TextView title= (TextView)getParent().findViewById(R.id.title);
			title.setText("Sentence Generation > Actions ");
  	}
		  }else if(SentenceGeneration.mTabHost.getCurrentTab()==0){ 
			  if (!HopeApplication.things.isEmpty()) {
			  		
		  			if (HopeApplication.things.size() <= 1) {
		  				ThingsActivity.thingsGrid.setAdapter(new Things(
		  						ThingsActivity.thingsactivitycontext,
		  						getLayoutInflater()));
		  			
		  				HopeApplication.things.remove(HopeApplication.things.get(HopeApplication.things.size() - 1));

		  				TextView tv1= (TextView)getParent().findViewById(R.id.acc);
						HopeApplication.object="";
						HopeApplication.verb="";
						 tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);

		  			} else {

		  				HopeApplication.things.remove(HopeApplication.things.get(HopeApplication.things.size() - 1));
		  				ThingsActivity.thingsGrid.setAdapter(new Things(
		  						ThingsActivity.thingsactivitycontext,
		  						getLayoutInflater(), HopeApplication.things
		  								.get(HopeApplication.things.size() - 1)));
		  				TextView tv1= (TextView)getParent().findViewById(R.id.acc);
						HopeApplication.object=HopeApplication.things.get(HopeApplication.things.size()-1);
						 tv1.setText(HopeApplication.pronoun.toString()+" "+HopeApplication.verb+" "+HopeApplication.object);
		  			}

		  	} else {
		  		
		  		Toast.makeText(this, "Cannot go back",
		  				Toast.LENGTH_SHORT).show();

		  	}
		  	
		  	if(HopeApplication.things.size()>=1)
			{
				TextView title= (TextView)getParent().findViewById(R.id.title);
				title.setText("Sentence Generation > Things > "+HopeApplication.things.get(0));
				
			}
			else
			{
				TextView title= (TextView)getParent().findViewById(R.id.title);
				title.setText("Sentence Generation > Things ");
			}
		  }
	  }
	  protected void onDestroy() {
	        super.onDestroy();
	        final GridView grid = actionsGrid;
	        final int count = grid.getChildCount();
	        LinearLayout ll = null;
	        ImageView v = null;
	        for (int i = 0; i < count; i++) {
	        	ll = (LinearLayout)grid.getChildAt(i);
	        	v = (ImageView)ll.getChildAt(0); 
	            ((BitmapDrawable) v.getDrawable()).setCallback(null);
	        }
	  }      
	public void check() {
		// TODO Auto-generated method stub
//		Toast.makeText(ActionsActivity.this, "checked",
//				Toast.LENGTH_SHORT).show();
	}

	

		
	public void onItemShortClick(AdapterView<?> parent, View v,
				int position, long id) {
			
			TextView title= (TextView)getParent().findViewById(R.id.title);
			title.setText("Sentence Generation > Actions");
			HopeApplication.newSentenceToAdd = "";
			Cursor c = null;
			TextView tv = (TextView) v.findViewById(R.id.icon_text);
			GridView actionsGrid = (GridView) findViewById(R.id.ActionsGrid);
			if(HopeApplication.things.size()==0)//things=0 
			{
			if(HopeApplication.actions.size()==0)//things=0 and actions=0
			{
				if (tv.getText().toString().equalsIgnoreCase("verbs")) {//things=0 and actions=0
					c = HopeApplication.db.query(
							"verbs", null, null, null, null,
							null, null);
					HopeApplication.actions.add("verbs");							
					actionsGrid.setAdapter((ListAdapter) new Actions(
					actionsGrid.getContext(),
					getLayoutInflater(), "verbs"));

				}
				else if (tv.getText().toString().equalsIgnoreCase("comments")) {//things=0 and actions=0

					c = HopeApplication.db.query("comment_category", null,
							null, null, null, null, null);
					HopeApplication.actions.add("comments");							
					actionsGrid.setAdapter((ListAdapter) new Actions(
					actionsGrid.getContext(),
					getLayoutInflater(), "comment_category"));

				}
				else if (tv.getText().toString().equalsIgnoreCase("questions")) {//things=0 and actions=0
					c = HopeApplication.db.query("questions", null,
							null, null, null, null, null);
					HopeApplication.actions.add("questions");							
					actionsGrid.setAdapter((ListAdapter) new Actions(
					actionsGrid.getContext(),
					getLayoutInflater(), "questions"));
				}
				else{
					//Code only when it is from a search result
					if(HopeApplication.pronoun.equals("")){
						HopeApplication.pronoun="I";
					}
					 if(searchTable!=null && !searchTable.equals("")){
						if(searchTable.equalsIgnoreCase("verbs"))//things=0 and actions=1
						{
							HopeApplication.verb=tv.getText().toString();
							HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
						}
						
						else if(searchTable.equalsIgnoreCase("comments"))//things=0 and actions=2
							{
								
								HopeApplication.verb=tv.getText().toString();//.substring(0,strsize-9
								HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
							}
							
						
							else if(searchTable.equalsIgnoreCase("questions"))//things=0 and actions=1
						{
							HopeApplication.question=tv.getText().toString();
							c = HopeApplication.db.query(
									"verbs", null, null, null, null,
									null, null);
							HopeApplication.actions.add("questions");
							HopeApplication.actions.add("verbs");							
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "verbs"));	
						}
				}
				}
			}
			else//things=0 and actions>0
			{
				if(HopeApplication.actions.size()==1)//things=0 and actions=1
				{
					if(HopeApplication.actions.get(0).equalsIgnoreCase("verbs"))//things=0 and actions=1
					{
						HopeApplication.verb=tv.getText().toString();
						HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
					}
					if(HopeApplication.actions.get(0).equalsIgnoreCase("comments"))//things=0 and actions=1
					{
						Cursor c1 = HopeApplication.db
						.rawQuery(
								"select * from things_comments where things='"
										+ HopeApplication.currentDisplayedActions.get(position)
										+ "'", null);
						HopeApplication.actions
						.add(HopeApplication.currentDisplayedActions
								.get(position));
						actionsGrid.setAdapter((ListAdapter) new Actions(
								actionsGrid.getContext(),
								getLayoutInflater(), c1,
								"comments"));
						
					}
					if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))//things=0 and actions=1
					{
						c = HopeApplication.db.query(
								"verbs", null, null, null, null,
								null, null);
						HopeApplication.actions.add("verbs");							
						actionsGrid.setAdapter((ListAdapter) new Actions(
						actionsGrid.getContext(),
						getLayoutInflater(), "verbs"));	
						//Toast.makeText(con, "questi", Toast.LENGTH_SHORT).show();
						HopeApplication.question=tv.getText().toString();
					}
				}
				else//things 0 action>1
				{
				if(HopeApplication.actions.size()==2)//things=0 and actions=2
				{
					if(HopeApplication.actions.get(0).equalsIgnoreCase("comments"))//things=0 and actions=2
					{
						//int strsize=tv.getText().toString().length();
						HopeApplication.verb=tv.getText().toString();//.substring(0,strsize-9
						HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
					}
					if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))//things=0 and actions=2
					{
						HopeApplication.verb=tv.getText().toString();
						HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
					}
				}
				}
				
			}
				
			}
			else//things>0 
			{ 
				if(HopeApplication.actions.size()==0)//things>0 actions=0
				{
					//For search
					if(HopeApplication.searchThings){
						if (tv.getText().toString().equalsIgnoreCase("verbs")) {//things=0 and actions=0
							c = HopeApplication.db.query(
									"verbs", null, null, null, null,
									null, null);
							HopeApplication.actions.add("verbs");							
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "verbs"));
					
							

						}
						if (tv.getText().toString().equalsIgnoreCase("comments")) {//things=0 and actions=0
							hasComeHere=true;
							c = HopeApplication.db.query("comments", null,
									null, null, null, null, null);
							HopeApplication.actions.add("comments");		//Change1					
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "comments"));

						}
						if (tv.getText().toString().equalsIgnoreCase("questions")) {//things=0 and actions=0
							c = HopeApplication.db.query("questions", null,
									null, null, null, null, null);
							HopeApplication.actions.add("questions");							
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "questions"));
							

						}
					}else{
					if (tv.getText().toString().equalsIgnoreCase("verbs")) {//things>0 actions=0
						
						Cursor c1 = HopeApplication.db
						.rawQuery(
								"select * from things_verbs where things='"
										+ HopeApplication.things.get(0)
										+ "'", null);
						//Toast.makeText(con, "here "+HopeApplication.things.get(0)+" "+c1.getCount(), Toast.LENGTH_SHORT).show();
						HopeApplication.actions
						.add("verbs");
				
						
						actionsGrid.setAdapter((ListAdapter) new Actions(
								actionsGrid.getContext(),
								getLayoutInflater(), c1,
								"verbs"));
					}
					
						if (tv.getText().toString().equalsIgnoreCase("comments")) {//things>0 actions=0
							Cursor c1 = HopeApplication.db
							.rawQuery(
									"select * from things_comments where things='"
											+ HopeApplication.things.get(0)
											+ "'", null);
							HopeApplication.actions
							.add("comments");
					
							
							actionsGrid.setAdapter((ListAdapter) new Actions(
									actionsGrid.getContext(),
									getLayoutInflater(), c1,
									"comments"));
						}
						if (tv.getText().toString().equalsIgnoreCase("questions")) {//things>0 actions=0
							c = HopeApplication.db.query("questions", null,
									null, null, null, null, null);
							HopeApplication.actions.add("questions");							
							actionsGrid.setAdapter((ListAdapter) new Actions(
							actionsGrid.getContext(),
							getLayoutInflater(), "questions"));
						}
					}
			}
				else
				{
				if(HopeApplication.actions.size()==1)//things>0 actions=1
				{
					if (HopeApplication.actions.get(0).equalsIgnoreCase("verbs")) {
					HopeApplication.verb=tv.getText().toString();
					HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
					}
					
					if (HopeApplication.actions.get(0).equalsIgnoreCase("comments")) {
						
						HopeApplication.verb=tv.getText().toString();
						HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
						
					}
					if (HopeApplication.actions.get(0).equalsIgnoreCase("questions")) {
						if(!HopeApplication.searchThings){
							
						
						Cursor c1 = HopeApplication.db.rawQuery(
								"select * from things_verbs where things='"
										+ HopeApplication.things.get(0)
										+ "'", null);
						HopeApplication.actions.add("verbs");
						
						HopeApplication.question=tv.getText().toString();//added to have question displayed on top
						
						actionsGrid.setAdapter((ListAdapter) new Actions(
								actionsGrid.getContext(),
								getLayoutInflater(), c1,
								"verbs"));
						}
						else{
						//Toast.makeText(con, "here "+HopeApplication.things.get(0)+" "+c1.getCount(), Toast.LENGTH_SHORT).show();
						HopeApplication.actions.add("verbs");
				
						HopeApplication.question=tv.getText().toString();//added to have question displayed on top
						
						actionsGrid.setAdapter((ListAdapter) new Actions(
								actionsGrid.getContext(),
								getLayoutInflater(), 
								"verbs"));
						}
					}
				}
				else if(HopeApplication.actions.size()==2)//things>0 actions=2
				{
					
					if (HopeApplication.actions.get(1).equalsIgnoreCase("verbs")) {
					HopeApplication.verb = tv.getText().toString();
					
					HopeApplication.selectedActionTable=HopeApplication.currentDisplayedActions.get(position);
					}
				}
				
				}
			}
			TextView tv1=(TextView)getParent().findViewById(R.id.acc);
			if(HopeApplication.actions.size()>0 )
			{
				if(HopeApplication.actions.get(0).equalsIgnoreCase("questions"))
				{
					HopeApplication.sentenceType="question";
				}
				else
				{
					HopeApplication.sentenceType="normal";
				}
			}
			if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
			{
				
				tv1.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
				
			}
			

			if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
			{
				
				tv1.setText( HopeApplication.question+" do "+HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object + " ? ");
				
				
			}
			
		}
		
		
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			super.onActivityResult(requestCode, resultCode, data);
			//Code for back button
			ImageButton bck = (ImageButton) getParent().findViewById(R.id.back);
			bck.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					backCode();
				}
			});
			
			if(resultCode==1)
			{
				TextView tv1=(TextView)getParent().findViewById(R.id.acc);
				if(HopeApplication.sentenceType.equalsIgnoreCase("normal"))
				{
					
					tv1.setText( HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object);
					
				}
				

				if(HopeApplication.sentenceType.equalsIgnoreCase("question"))
				{
					
					tv1.setText( HopeApplication.question+" do "+HopeApplication.pronoun+" "+HopeApplication.verb+" "+ HopeApplication.object
							+ " ? ");
					
					
				}
			}
			
		
}
}

